const server = require("http").createServer()
const io = require("socket.io")(server)
const Controller = require('./controller.js')

io.on('connection', socket => {

    socket.on('start', () => Controller.ConnectToWA(socket))

    socket.on('logout', () => Controller.LogoutFromWA(socket))

})

server.on("error", (err) => {
    console.log("Error opening server")
})

server.listen(3000, () => {
    console.log("Server working on port 3000")
})
