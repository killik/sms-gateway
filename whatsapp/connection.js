const {
	default: makeWASocket,
	useSingleFileAuthState,
	DisconnectReason
} = require('@adiwajshing/baileys')

const { state, saveState } = useSingleFileAuthState(__dirname + '/auth.json')

const sock = makeWASocket({
	printQRInTerminal: false,
	auth: state
})

sock.ev.on("creds.update", saveState);

module.exports = {
	sock,
	DisconnectReason
}