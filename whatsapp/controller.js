const { sock, DisconnectReason } = require('./connection.js')
const QRCode = require('qrcode')
const fs = require('fs')

function ConnectToWA(socket) {
	console.log('------ socket:start');
	socket.emit('message', 'Connecting...')

	sock.ev.on('connection.update', (update) => {
		const { connection, lastDisconnect, qr } = update

		if (connection) {
			console.log('------ socket:connection');
			socket.emit('message', `Connection: ${connection}`)
		}

		if (qr) {
			QRCode.toDataURL(qr, (err, url) => {
				console.log('------ socket:qr');
				socket.emit('qr', url)
				socket.emit('message', 'QR Code received, scan please!')
			})
		}

		if (connection === 'close') {
			const shouldReconnect = lastDisconnect.error?.output?.statusCode !== DisconnectReason.loggedOut
			socket.emit('message', `Connection closed due to "${lastDisconnect.error}",  ${shouldReconnect ? "reconnecting..." : "not reconnecting."}`)
			// reconnect if not logged out
			// if(shouldReconnect) {
			// 	console.log('------ debug:reconnect');
			// 	ConnectToWA(socket)
			// } else {
				console.log('------ debug:rm_auth');
				socket.emit('message', 'Logging out...')
				if (fs.existsSync(__dirname + '/auth.json')) {
					fs.rmSync(__dirname + '/auth.json')
				}
			// }
		}
		else if (connection === 'open') {
			console.log('------ socket:ready');
			socket.emit('message', 'Connected')
			socket.emit('ready')
		}
	})
}

function LogoutFromWA(socket) {
	console.log('------ debug:rm_auth');
	socket.emit('message', 'Logging out...')
	if (fs.existsSync(__dirname + '/auth.json')) {
		fs.rmSync(__dirname + '/auth.json')
	}
}

module.exports = {
	ConnectToWA,
	LogoutFromWA
}