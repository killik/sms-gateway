# portalsms

Dashboard untuk manjemen API gateway SMS & Whatsapp.

# Installation

1. Install seperti laravel biasa
	- `cp .env.example .env` kemudian sesuaikan config database-nya
	- `php artisan key:generate`
	- `php artisan migrate`
	- `php artisan db:seed` lalu isi akun super

2. Jalankan worker menggunakan PM2
	```
	# Installasi pm2 jika belum ada
	npm i -g pm2
	# Jalankan worker dengan pm2
	pm2 start worker.yml
	```