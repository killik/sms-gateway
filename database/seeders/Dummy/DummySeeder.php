<?php

namespace Database\Seeders\Dummy;

use Database\Seeders\Dummy\Auth\AuthSeeder;
use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthSeeder::class);
    }
}
