<?php

namespace Database\Seeders;

use Database\Seeders\Dummy\DummySeeder as DummyDummySeeder;
use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DummyDummySeeder::class);
    }
}
