<?php

namespace Database\Seeders\System;

use Database\Seeders\System\Auth\AuthSeeder;
use Database\Seeders\System\Generic\MetaSeeder;
use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthSeeder::class);
        $this->call(MetaSeeder::class);
    }
}
