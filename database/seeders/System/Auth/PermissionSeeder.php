<?php

namespace Database\Seeders\System\Auth;

use App\Models\Auth\Permission\Role;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([

            "name"          => "super",
            "guard_name"    => "web"
        ]);
    }
}
