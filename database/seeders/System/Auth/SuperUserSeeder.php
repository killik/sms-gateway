<?php

namespace Database\Seeders\System\Auth;

use App\Models\Auth\Permission\Role;
use App\Models\Auth\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class SuperUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Carbon $carbon)
    {
        $this->command->newLine(1);
        $this->command->info('Membuat akun super');

        $role   = Role::whereName('super')->whereGuardName('web')->first();

        $user   = User::create([
            'name'              => $this->command->ask('Name', 'Super User'),
            'email'             => $this->command->ask('Email', 'root@local.host'),
            'email_verified_at' => $carbon->now(),
            'password'          => Hash::make($this->command->ask('Password', 'password')),
            'phone'             => $this->command->ask('Phone', '08123456789'),
        ]);

        $user->assignRole($role);
    }
}
