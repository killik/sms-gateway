<?php

namespace Database\Seeders\System\Generic;

use App\Models\Generic\Meta;
use Illuminate\Database\Seeder;

class MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->newLine(1);
        $this->command->info('Membuat meta system');

        Meta::create([

            'key'   => 'system_config',
            'data'  => [

                'descriptiom'   => 'Pengaturan system',
                'quota_price'   => '100'
            ]
        ]);
    }
}
