window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');

    ! function (s) {
        "use strict";
        s("#sidebarToggle, #sidebarToggleTop").on("click", function (e) {
            s("body").toggleClass("sidebar-toggled"), s(".sidebar").toggleClass("toggled"), s(".sidebar").hasClass("toggled") && s(".sidebar .collapse").collapse("hide")
        }), s(window).resize(function () {
            s(window).width() < 768 && s(".sidebar .collapse").collapse("hide"), s(window).width() < 480 && !s(".sidebar").hasClass("toggled") && (s("body").addClass("sidebar-toggled"), s(".sidebar").addClass("toggled"), s(".sidebar .collapse").collapse("hide"))
        }), s("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function (e) {
            if (768 < s(window).width()) {
                var o = e.originalEvent,
                    l = o.wheelDelta || -o.detail;
                this.scrollTop += 30 * (l < 0 ? 1 : -1), e.preventDefault()
            }
        }), s(document).on("scroll", function () {
            100 < s(this).scrollTop() ? s(".scroll-to-top").fadeIn() : s(".scroll-to-top").fadeOut()
        }), s(document).on("click", "a.scroll-to-top", function (e) {
            var o = s(this);
            s("html, body").stop().animate({
                scrollTop: s(o.attr("href")).offset().top
            }, 1e3, "easeInOutExpo"), e.preventDefault()
        })
    }(window.jQuery);
} catch (e) { }

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.Chart = require('chart.js');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });

import 'alpinejs'

