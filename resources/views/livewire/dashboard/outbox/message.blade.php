@php

$colors = [
    'pending' => 'warning',
    'processing' => 'primary',
    'sent' => 'success',
    'failed' => 'danger',
    'queued' => 'primary',
];

@endphp

<tr class="text-{{ $colors[$message->status] }}">
    <td class="text-center">{{ $message->contact->name ?? $message->destination }}</td>
    <td class="text-center">{{ $message->created_at->isoFormat('D MMMM Y - HH:mm:ss') }}</td>
    <td>{{ $message->message }}</td>
    <td class="text-center">
        <span class="badge badge-{{ $colors[$message->status] }}">
            {{ $message->status }}
        </span>
        @if (!empty($message->gateway_id))
            <span type="button" class="badge badge-primary" wire:click="reload">
                Reload Status
            </span>
        @endif

        @if (Str::contains($message->status, 'failed'))
            <span type="button" class="badge badge-{{ $colors[$message->status] }}" wire:click="send">
                Kirim ulang
            </span>
        @endif
    </td>
</tr>
