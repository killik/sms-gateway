<div>
    <div class="row justify-content-center">
        <div class="col-md-4 col-sm-12 mb-3">
            <input type="search" class="form-control shadow" placeholder="Search {{ $status ?? 'outbox' }} message"
                wire:model="keyword" wire:input="resetPage">
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Pesan Keluar</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-center text-uppercase" width="15%">Tujuan</th>
                            <th class="text-center text-uppercase" width="15%">Waktu Kirim</th>
                            <th class="text-center text-uppercase">Isi Pesan</th>
                            <th class="text-center text-uppercase" width="15%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($outbox as $item)
                            <livewire:dashboard.outbox.message-whatsapp :key="$item->id" :message="$item" />
                        @empty
                            <tr>
                                <td class="text-center" colspan="4">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
		<div class="justify-content-center row">
			{!! $outbox->links('livewire.dashboard.pagination') !!}
		</div>
    </div>    
</div>
