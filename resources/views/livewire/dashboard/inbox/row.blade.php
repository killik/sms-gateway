<div>
    <div class="row justify-content-center">
        <div class="col-md-4 col-sm-12 mb-3">
            <input type="search" class="form-control shadow" placeholder="Search {{ $status ?? 'inbox' }} message" wire:model="keyword"
                wire:input="resetPage">
        </div>
    </div>

    <div class="row justify-content-center">

        @foreach ($inbox as $item)

            <livewire:dashboard.inbox.message :key="$item->id" :message="$item" />

        @endforeach

    </div>

    <div class="justify-content-center row">
        {!! $inbox->links('livewire.dashboard.pagination') !!}
    </div>
</div>
