@php

$colors = [

'pending' => 'secondary',
'processing' => 'primary',
'sent' => 'success',
'failed' => 'danger',
'queued' => 'primary'
];

@endphp

<div class="col-sm-6 col-md-4 mb-3">
    <div class="card border-left-primary shadow h-100 py-0">
        <header class="card-header py-2">
            <div class="text-xs text-primary text-uppercase">

                {{ $message->contact->name ?? $message->destination }}

                <span class="float-right">
                    {{ $message->created_at->format('d-m-Y') }}
                </span>
            </div>
        </header>
        <article class="card-body py-1">
            <div class="mr-2">
                <div class="mb-0 text-gray-800">

                    {{ $message->message }}

                </div>
            </div>
        </article>
        <footer class="card-footer py-2">
            <div class="mb-0 float-right">

                @if (!empty($message->gateway_id))
                    <span type="button" class="badge badge-primary" wire:click="reply">

                        Balas

                    </span>
                @endif

            </div>
        </footer>
    </div>
</div>
