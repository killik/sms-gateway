@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="page-item">
                    <button type="button" dusk="previousPage" class="page-link" wire:click="previousPage"
                        wire:loading.attr="disabled" rel="prev"
                        aria-label="@lang('pagination.previous')">&lsaquo;</button>
                </li>
            @endif

            @if ($paginator->currentPage() > 3)
                <li class="page-item" wire:key="paginator-page-1">
                    <button type="button" class="page-link" wire:click="gotoPage(1)">
                        1
                    </button>
                </li>
            @endif
            @if ($paginator->currentPage() > 4)
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">...</span>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach (range(1, $paginator->lastPage()) as $page)
                {{--
                @if ($page >= $paginator->currentPage() - 1 && $page <= $paginator->currentPage() + 1)
                    --}}

                    @if ($page == $paginator->currentPage())
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" wire:key="paginator-page-{{ $page }}" aria-current="page">
                                <span class="page-link">{{ $page }}</span>
                            </li>
                        @else
                            <li class="page-item" wire:key="paginator-page-{{ $page }}">
                                <button type="button" class="page-link" wire:click="gotoPage({{ $page }})">
                                    {{ $page }}
                                </button>
                            </li>
                        @endif
                    @endif
                @endforeach


                @if ($paginator->currentPage() < $paginator->lastPage() - 3)
                    <li class="page-item disabled" aria-disabled="true">
                        <span class="page-link">...</span>
                    </li>
                @endif
                @if ($paginator->currentPage() < $paginator->lastPage() - 2)
                    <li class="page-item" wire:key="paginator-page-{{ $paginator->lastPage() }}">
                        <button type="button" class="page-link" wire:click="gotoPage({{ $paginator->lastPage() }})">
                            {{ $paginator->lastPage() }}
                        </button>
                    </li>
                @endif

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="page-item">
                        <button type="button" dusk="nextPage" class="page-link" wire:click="nextPage"
                            wire:loading.attr="disabled" rel="next"
                            aria-label="@lang('pagination.next')">&rsaquo;</button>
                    </li>
                @else
                    <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                        <span class="page-link" aria-hidden="true">&rsaquo;</span>
                    </li>
                @endif
        </ul>
    </nav>
@endif
