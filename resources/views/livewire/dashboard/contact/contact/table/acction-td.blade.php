<td scope="col" class="drodown overflow-visible">
    <a type="button" class="dropdown-toggle text-decoration-none" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <i class="fas fa-tools"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item dropdown" wire:click="edit">
            Edit
        </a>
        <a class="dropdown-item dropdown" wire:click="sms">
            SMS
        </a>
        <a class="dropdown-item dropdown" wire:click="delete">
            Hapus
        </a>
    </div>
</td>
