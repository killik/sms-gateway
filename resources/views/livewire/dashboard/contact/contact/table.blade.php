<div>
    @php $num = 1; @endphp

    <div class="row">
        <div class="col-md-4 col-sm-12 mb-3">
            <div class="input input-group">
                <input type="text" class="form-control shadow " placeholder="Ketik pesan"
                    wire:model.debounce.500ms="form">
                <div class="input-group-append dropdown shadow">
                    <button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-tools"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a type="button" class="dropdown-item dropdown" data-toggle="modal"
                            data-target="#createContactModalDashboardContactContact">
                            Tambah Kontak
                        </a>

                        <a type="button" class="dropdown-item dropdown" data-toggle="modal"
                            data-target="#importContactModalDashboardContactContactImport">
                            Import Kontak
                        </a>

                        <a class="dropdown-item dropdown" type="button" wire:click="export">
                            Export Kontak
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table table-responsive">
        <table class="table table-sm text-center text-nowrap">
            <thead class="thead-dark">
                <tr>
                    <th class="text-center text-uppercase" scope="col">No.</th>
                    <th class="text-center text-uppercase" scope="col">Nomor Telepon</th>
                    <th class="text-center text-uppercase" scope="col">Nama</th>
                    <th class="text-center text-uppercase" scope="col">Pesan Keluar</th>
                    <th class="text-center text-uppercase" scope="col">Pesan Masuk</th>
                    <th class="text-center text-uppercase" scope="col">Grup</th>
                    <th class="text-center text-uppercase" scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>

                @forelse ($contacts as $contact)

                    <tr scope="row">
                        <th scope="col">{{ $num++ }}</th>
                        <td scope="col" class="text text-monospace">
                            {{ $contact->phone_number }}
                        </td>
                        <td scope="col">{{ $contact->name }}</td>
                        <td scope="col">{{ $contact->outbox()->count() }}</td>
                        <td scope="col"> - </td>
                        <td scope="col"> - </td>

                        <livewire:dashboard.contact.contact.table.acction-td :contact="$contact" :key="$contact->id" />
                    </tr>

                @empty
                    <tr scope="row">
                        <td colspan="7" class="text-center">Tidak ada data</td>
                    </tr>
                @endforelse
                </tr>
            </tbody>
        </table>
    </div>
    <div class="justify-content-center row">
        {!! $contacts->links('livewire.dashboard.pagination') !!}
    </div>
</div>
