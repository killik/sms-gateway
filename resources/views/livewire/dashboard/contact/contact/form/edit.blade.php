<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="editContactModalDashboardContactContactLabelInputNama">
            Name
        </label>
        <input type="text" class="form-control @error('name') is-invalid @enderror"
            id="editContactModalDashboardContactContactLabelInputNama"
            aria-describedby="editContactModalDashboardContactContactLabelInputNamaHelp" placeholder="Masukan nama"
            wire:model.debounce.500ms="name">

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="editContactModalDashboardContactContactLabelInputNamaHelp" class="form-text text-muted">
                Shure name.
            </small>
        @enderror

    </div>
    <div class="form-group">
        <label for="editContactModalDashboardContactContactLabelInputPhoneNumber">Phone Number</label>
        <input type="phone" class="form-control @error('phone_number') is-invalid @enderror"
            id="editContactModalDashboardContactContactLabelInputPhoneNumber"
            aria-describedby="editContactModalDashboardContactContactLabelInputPhoneNumberHelp" placeholder="Masukan phone number"
            wire:model.debounce.500ms="phone_number">
        @error('phone_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="editContactModalDashboardContactContactLabelInputPhoneNumberHelp" class="form-text text-muted">
                We'll never share the phone number with anyone else.
            </small>
        @enderror

    </div>
    
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-save" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-save"></i>
        </span>
        <span class="text">Simpan</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times"></i>
        </span>
        <span class="text">Reset</span>
    </button>
		
</form>
