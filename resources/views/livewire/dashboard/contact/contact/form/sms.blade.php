<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="smsContactModalDashboardContactContactLabelInputDestination">
            Tujuan
        </label>
        <input type="numeric" class="form-control @error('destination') is-invalid @enderror"
            id="smsContactModalDashboardContactContactLabelInputDestination"
            aria-describedby="smsContactModalDashboardContactContactLabelInputDestinationHelp"
            placeholder="Masukan nama" wire:model.debounce.500ms="destination">

        @error('destination')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="smsContactModalDashboardContactContactLabelInputDestinationHelp" class="form-text text-muted">
                Pesan.
            </small>
        @enderror

    </div>
    <div class="form-group">
        <label for="smsContactModalDashboardContactContactLabelInputMessager">Isi Pesan</label>
        <textarea class="form-control @error('message') is-invalid @enderror"
            id="smsContactModalDashboardContactContactLabelInputMessager"
            aria-describedby="smsContactModalDashboardContactContactLabelInputMessagerHelp"
            placeholder="Write message here" wire:model.debounce.500ms="message">
        </textarea>
        @error('message')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="smsContactModalDashboardContactContactLabelInputMessagerHelp" class="form-text text-muted">
                We'll never share the phone number with anyone else.
            </small>
        @enderror

    </div>
    @if ($user->sms < 1)
        <p>
            <small id="smsContactModalDashboardContactContactLabelInputMessagerHelp" class="form-text text-danger">
                We'll never share the phone number with anyone else.
            </small>
        </p>
    @endif
	
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm" @if ($user->sms < 1) disabled @endif>
        <span class="icon text-white-50">
            <i class="fas fa-paper-plane" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-telegram"></i>
        </span>
        <span class="text">Simpan</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times"></i>
        </span>
        <span class="text">Reset</span>
    </button>
	
</form>
