<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="createContactModalDashboardContactContactLabelInputNama">
            Nama
        </label>
        <input type="text" class="form-control @error('name') is-invalid @enderror"
            id="createContactModalDashboardContactContactLabelInputNama"
            aria-describedby="createContactModalDashboardContactContactLabelInputNamaHelp" placeholder="Masukan nama"
            wire:model.debounce.500ms="name">

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="createContactModalDashboardContactContactLabelInputNamaHelp" class="form-text text-muted">
                Nama Lengkap
            </small>
        @enderror

    </div>
    <div class="form-group">
        <label for="createContactModalDashboardContactContactLabelInputPhoneNumber">Phone Number</label>
        <input type="phone" class="form-control @error('phone_number') is-invalid @enderror"
            id="createContactModalDashboardContactContactLabelInputPhoneNumber"
            aria-describedby="createContactModalDashboardContactContactLabelInputPhoneNumberHelp" placeholder="Masukan phone number"
            wire:model.debounce.500ms="phone_number">
        @error('phone_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="createContactModalDashboardContactContactLabelInputPhoneNumberHelp" class="form-text text-muted">
                Kami tidak akan pernah membagikan nomor telepon ini kepda pihak lain.
            </small>
        @enderror

    </div>
    
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-save" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-save"></i>
        </span>
        <span class="text">Simpan</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times"></i>
        </span>
        <span class="text">Reset</span>
    </button>
	
</form>
