<form wire:submit.prevent="submit">
    <p>
        Anda yakin akan menghapus <b>{{ $name }}</b>[<b>{{ $phoneNumber }}</b>] dari Kontak?
    </p>
	
    <button type="submit" class="btn btn-danger btn-icon-split btn-sm">
		<span class="icon text-white-50">
			<i class="fas fa-trash" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-trash"></i>
        </span>
        <span class="text">Hapus</span>
    </button>
    
	<button type="button" class="btn btn-success btn-icon-split btn-sm pull-right" wire:click="cancel">
        <span class="icon text-white-50">
            <i class="fas fa-times"></i>
        </span>
        <span class="text">Batal</span>
    </button>
</form>
