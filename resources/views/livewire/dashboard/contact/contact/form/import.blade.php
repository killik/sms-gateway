<form wire:submit.prevent="submit">
    <div class="form-group custom-file">
        <label for="dataImportContactModalDashboardContactContactImport" class="custom-file-label">File data</label>
        <input type="file" class="custom-file-input @error('data') is-invalid @enderror" id="dataImportContactModalDashboardContactContactImport"
            wire:model="data">

        @error('data')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>

        @else

            <small id="dataImportContactModalDashboardContactContactImportHelp" class="form-text text-muted">
                @if (isset($contacts))

                    Ditemukan {{ $contacts->count() }} kontak

                @else

                    Import kontak, nomor wajib dengan kode negara ex: +62, format .txt Nama;Nomor

                @endif
            </small>

        @enderror


    </div>

    <button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-save" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-save"></i>
        </span>
        <span class="text">Simpan</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times"></i>
        </span>
        <span class="text">Reset</span>
    </button>
</form>
