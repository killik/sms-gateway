@php

$number = 1;

$colors = [

'pending' => 'warning',
'processing' => 'primary',
'accepted' => 'success',
'rejected' => 'danger',
'closed' => 'secondary'
];

@endphp


<div>
    <div class="row">
        <div class="col-md-4 col-sm-12 mb-3">
            <div class="input input-group">
                <input type="text" class="form-control shadow " placeholder="Ketik pesan"
                    wire:model.debounce.500ms="form">
                <div class="input-group-append shadow">
                    <button class="btn btn-outline-secondary" type="submit">
                        <i class="fas fa-plus"></i>
                        New
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="table table-responsive">
        <table class="table table-sm shadow text-center text-nowrap">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Group</th>
                    <th scope="col">Created At</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>

                @forelse ($groups as $group)

                    <tr scope="row">
                        <th scope="col">{{ $number++ }}</th>
                        <td scope="col">{{ $group->name }}</td>
                        <td scope="col">{{ $group->contacts()->count() }}</td>
                        <td scope="col"> - </td>
                        <td scope="col">{{ $group->created_at->format('d-m-Y') }}</td>
                        <td scope="col">{{ $group->created_at->format('d-m-Y') }}</td>
                    </tr>
                @empty
                    <tr scope="row">
                        <td colspan="7" class="text-center">Tidak ada data</td>
                    </tr>
                @endforelse
                </tr>
            </tbody>
        </table>
    </div>
    <div class="justify-content-center row">
        {!! $groups->links('livewire.dashboard.pagination') !!}
    </div>
</div>
