<li class="nav-item dropdown no-arrow">
    <a class="nav-link dropdown-toggle" href="{{ route('dashboard.invoice.index', ['status' => 'pending']) }}" role="button">
        <i class="fas fa-dollar-sign fa-fw"></i>
        <!-- Counter - Alerts -->
        @if ($invoice > 0)
            <span class="badge badge-danger badge-counter">{{ $invoice }}</span>
        @endif
    </a>
</li>
