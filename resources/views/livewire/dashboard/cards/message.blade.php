@php
    if (Str::contains($message->status, 'failed'))
    {
        $color = 'danger';
    }

    elseif (Str::contains($message->status, 'sent'))
    {
        $color = 'success';
    }

    else
    {
        $color = 'primary';
    }
@endphp

<div class="col-xl-6 mb-3">
    <div class="card border-left-{{ $color }} shadow h-100 py-0">
        <div class="card-body py-3">
            <div class="mr-2">
                <div class="mb-1 text-xs text-{{ $color }} text-uppercase">

                    {{ $message->destination }}

                    {{-- <i class="fas fa-bars float-right"></i> --}}
                </div>
                <div class="mb-0 text-gray-800">

                    {{ $message->message }}

                </div>
            </div>
        </div>
    </div>
</div>
