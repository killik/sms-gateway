<div class="card shadow py-0 mb-3">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="mb-1">
                    <div class="text-xs text-danger text-uppercase mb-1">
                        Token Key SMS Gateway

                        <a type="button" class="text-danger text-decoration-none float-right" wire:click="new">

                            <i class="fas fa-redo" wire:loading.class="fa-spin"></i>

                            Buat Ulang
                        </a>
                    </div>
                </div>
                <div class="h5 text-gray-800" style="padding:10px;border:1px solid #ddd;border-radius:3px;margin-bottom:10px;margin-top:5px;">
                    <code>
                        {{ $user->api_token }}
                    </code>
                </div>
                <div class="alert alert-danger text-sm text-danger mt-1" style="margin-bottom:0;padding:5px 10px;">
                    <i class="fas fa-exclamation-circle"></i> Mengganti <strong>TOKEN KEY SMS GATEWAY</strong> mungkin akan dapat mengganggu layanan yang sudah berjalan.
                </div>
            </div>
        </div>
    </div>
</div>
