<div>
    <div class="row justify-content-center">

        <input type="search" class="form-control col-md-5 mb-3 mx-3" placeholder="Search..." wire:model="keyword" wire:input="resetPage">

        {!! $messages->links('livewire.dashboard.home.rows.messages.pagination') !!}


    </div>

    <div class="justify-content-center row">
        @forelse ($messages as $message)

            <livewire:dashboard.cards.message :key="$message->id" :message=$message />

        @empty

        <div class="card border-left-danger shadow h-100">
            <div class="card-body">
				<div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="mb-1 text-xs text-danger text-uppercase">
                            Kesalahan
                        </div>
                        <div class="mb-0 text-gray-800">
                            Tidak ada pesan yang cocok dengan kata kunci yang anda masukan.
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endforelse

    </div>
</div>
