<div class="row">

    <!-- Card kuota whatsapp -->
    <div class="col-xl-3 col-md-6 mb-3">
		<div class="small-box bg-happy-green">
            <div class="inner">
				<h3>Sisa Kuota WA</h3>
                <p>{{ $user->wa }}</p>
            </div>
            <div class="icon">
                <i class="bi bi-clock-history"></i>
            </div>
            <a type="button" class="small-box-footer text-decoration-none" data-toggle="modal" data-target="#quotaProfileModalDashboardProfile">Top Up Kuota <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!-- Card kuota SMS -->
	<div class="col-xl-3 col-md-6 mb-3">
		<div class="small-box bg-mixed-hopes">
            <div class="inner">
				<h3>Sisa Kuota SMS</h3>
                <p>{{ $user->sms }}</p>
            </div>
            <div class="icon">
                <i class="bi bi-clock-history"></i>
            </div>
            <a type="button" class="small-box-footer text-decoration-none" data-toggle="modal" data-target="#quotaProfileModalDashboardProfile">Top Up Kuota <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!-- Card profile -->
	<div class="col-xl-3 col-md-6 mb-3">
		<div class="small-box bg-happy-fisher">
            <div class="inner">
				<h3>Nama Pengguna</h3>
                <p>{{ $user->name }}</p>
            </div>
            <div class="icon">
                <i class="bi bi-person-check"></i>
            </div>
            <a type="button" class="small-box-footer text-decoration-none" data-toggle="modal" data-target="#editProfileModalDashboardProfile">Edit Profil <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
	<div class="col-xl-3 col-md-6 mb-3">
		<div class="small-box bg-sunny-morning">
            <div class="inner">
				<h3>Email</h3>
                <p>{{ $user->email }}</p>
            </div>
            <div class="icon">
                <i class="bi bi-shield-lock"></i>
            </div>
            <a type="button" class="small-box-footer text-decoration-none" data-toggle="modal" data-target="#cpassProfileModalDashboardProfile">Ganti Password <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
