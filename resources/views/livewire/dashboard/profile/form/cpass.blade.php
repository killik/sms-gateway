<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="cpassProfileModalDashboardProfileLabelInputPassword">
            Password Lama
        </label>
        <input type="password" class="form-control @error('password') is-invalid @enderror"
            id="cpassProfileModalDashboardProfileLabelInputPassword"
            aria-describedby="cpassProfileModalDashboardProfileLabelInputPasswordHelp" placeholder="Masukan password"
            wire:model.debounce.500ms="password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="cpassProfileModalDashboardProfileLabelInputPasswordHelp" class="form-text text-muted">
                Password Lama
            </small>
        @enderror

    </div>

    <div class="form-group">
        <label for="cpassProfileModalDashboardProfileLabelInputNPassword">
            Password Baru
        </label>
        <input type="password" class="form-control @error('n_password') is-invalid @enderror"
            id="cpassProfileModalDashboardProfileLabelInputNPassword"
            aria-describedby="cpassProfileModalDashboardProfileLabelInputNPasswordHelp" placeholder="Masukan password baru"
            wire:model.debounce.500ms="n_password">

        @error('n_password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="cpassProfileModalDashboardProfileLabelInputNPasswordHelp" class="form-text text-muted">
                Password Baru
            </small>
        @enderror

    </div>

    <div class="form-group">
        <label for="cpassProfileModalDashboardProfileLabelInputCPassword">Ulangi Password</label>
        <input type="password" class="form-control @error('c_password') is-invalid @enderror"
            id="cpassProfileModalDashboardProfileLabelInputCPassword"
            aria-describedby="cpassProfileModalDashboardProfileLabelInputCPasswordlHelp" placeholder="Ulangi Password"
            wire:model.debounce.500ms="c_password">
        @error('c_password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="cpassProfileModalDashboardProfileLabelInputCPasswordHelp" class="form-text text-muted">
                Ulangi password.
            </small>
        @enderror

    </div>
	
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-save" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-save" style="padding-top:3px"></i>
        </span>
        <span class="text">Simpan</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times" style="padding-top:3px"></i>
        </span>
        <span class="text">Reset</span>
    </button>
	
</form>
