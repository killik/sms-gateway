<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="paketProfileModalDashboardProfileLabelInputPaket">
            Paket
        </label>
        <select type="number" class="form-control @error('selected') is-invalid @enderror"
            id="paketProfileModalDashboardProfileLabelInputPaket"
            aria-describedby="paketProfileModalDashboardProfileLabelInputPaketHelp" placeholder="Masukan kuantitas"
            wire:model.debounce.500ms="selected">

            @forelse($products as $product)

                <option value="{{ $product->id }}">

                    {{ $product->name }} {{ $product->description }}

                </option>

            @empty

                <option>

                    Kosong

                </option>

            @endforelse

        </select>

        @error('selected')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>

        @else

            <small id="paketProfileModalDashboardProfileLabelInputPaketHelp" class="form-text text-muted">
                Tarif: Rp. {{ number_format($this->price, 0, ',', '.') }}<br />
				Untuk jumlah pembayaran silakan lihat pada <a href="{{ route('dashboard.invoice.index') }}">Invoice</a><br />
                Invoice akan ditutup 7 x 24 Jam setelah dibuat.
            </small>

        @enderror

    </div>
	
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-shopping-bag" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-shopping-bag" style="padding-top:3px"></i>
        </span>
        <span class="text">Beli Paket</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times" style="padding-top:3px"></i>
        </span>
        <span class="text">Reset</span>
    </button>
</form>
