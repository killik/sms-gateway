<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="editProfileModalDashboardProfileLabelInputNama">
            Nama
        </label>
        <input type="text" class="form-control @error('name') is-invalid @enderror"
            id="editProfileModalDashboardProfileLabelInputNama"
            aria-describedby="editProfileModalDashboardProfileLabelInputNamaHelp" placeholder="Masukan nama"
            wire:model.debounce.500ms="name">

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="editProfileModalDashboardProfileLabelInputNamaHelp" class="form-text text-muted">
                Nama Lengkap
            </small>
        @enderror

    </div>
    <div class="form-group">
        <label for="editProfileModalDashboardProfileLabelInputEmail">Email</label>
        <input type="email" class="form-control @error('email') is-invalid @enderror"
            id="editProfileModalDashboardProfileLabelInputEmail"
            aria-describedby="editProfileModalDashboardProfileLabelInputEmailHelp" placeholder="Masukan email"
            wire:model.debounce.500ms="email">
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @else
            <small id="editProfileModalDashboardProfileLabelInputEmailHelp" class="form-text text-muted">
               Kami tidak akan pernah membagikan email Anda dengan siapa pun lain.
            </small>
        @enderror

    </div>
    
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-save" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-save" style="padding-top:3px;"></i>
        </span>
        <span class="text">Simpan</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times" style="padding-top:3px;"></i>
        </span>
        <span class="text">Reset</span>
    </button>
		
</form>
