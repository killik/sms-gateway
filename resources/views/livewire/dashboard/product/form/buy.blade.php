<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="{{ $modal }}InputPaket">
            Paket
        </label>
        <select type="number" class="form-control @error('selected') is-invalid @enderror"
            id="{{ $modal }}InputPaket"
            aria-describedby="{{ $modal }}InputPaketHelp" placeholder="Masukan kuantitas"
            wire:model="selected">

            @forelse($products as $product)

                <option value="{{ $product->id }}">

                    {{ $product->name }} {{ $product->description }}

                </option>

            @empty

                <option>

                    Kosong

                </option>

            @endforelse

        </select>

        @error('selected')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>

        @else

            <small id="{{ $modal }}InputPaketHelp" class="form-text text-muted">
                <strong>Tarif</strong>: Rp. {{ number_format($this->price, 0, ',', '.') }}<br />
				Untuk jumlah pembayaran silakan lihat pada <a href="{{ route('dashboard.invoice.index') }}">Invoice</a><br />
                Invoice akan ditutup 7 x 24 Jam setelah dibuat.
            </small>

        @enderror

    </div>
	
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-shopping-bag" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-shopping-bag"></i>
        </span>
        <span class="text">Beli Paket</span>
    </button>
	
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="cancel">
        <span class="icon text-white-50">
            <i class="fas fa-times"></i>
        </span>
        <span class="text">Cancel</span>
    </button>
</form>
