<a type="button" class="btn btn-primary btn-icon-split btn-sm" wire:click="buy" aria-expanded="false">
    <span class="icon text-white-50">
        <i class="fas fa-shopping-bag" style="padding-top: 3px;"></i>
    </span>
    <span class="text">Beli Paket</span>
</a>
