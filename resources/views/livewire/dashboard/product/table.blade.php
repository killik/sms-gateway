@php

$number = 1;

$colors = [
    'active' => 'primary',
    'inactive' => 'secondary',
];

@endphp

<div class="row justify-content-center mb-3">
	@forelse ($products as $product)
	<div class="col-md-4 mb-3">
		<div class="card shadow h-100 py-0">
			<div class="card-body py-3">
				<div class="mb-3 text-uppercase text-center" style="padding:10px 0;background:#ddd;font-weight:700">
						 {{ $product->name }}
					</div>
					<div class="mb-0 text-gray-800" style="padding:10px 0;border-bottom:1px solid #ddd;">
						<span style="font-weight:700">Kuota SMS</span> <span style="float:right">{{ $product->sms }} Pesan</span>
					</div>
					<div class="mb-0 text-gray-800" style="padding:10px 0;border-bottom:1px solid #ddd;">
						<span style="font-weight:700">Kuota WhatsApp</span> <span style="float:right">{{ $product->wa }} Pesan</span>
					</div>
					<div class="mb-0 text-gray-800" style="padding:10px 0;border-bottom:1px solid #ddd;">
						<span style="font-weight:700">Kuota Telegram</span> <span style="float:right">0 Pesan</span>
					</div>
					<div class="mb-0 text-gray-800" style="padding:10px 0;border-bottom:1px solid #ddd;">
						<span style="font-weight:700">Tarif</span> <span style="float:right">Rp. {{ number_format($product->price, 0, ',', '.') }}</span>						
					</div>
					<div class="mb-0 text-gray-800" style="padding:10px 0;border-bottom:1px solid #ddd;">
						<span style="font-weight:700">Masa Aktif</span> <span style="float:right">Unlimited</span>						
					</div>
					<div class="mt-3 text-center">
						<livewire:dashboard.product.table.acction-td :product="$product" :key="$product->id" />
					</div>
			</div>
		</div>	
	</div>
	@empty
		<div class="text-center">Tidak ada data</div>
	@endforelse

</div>
