@php

$number = 1;

$colors = [

'pending' => 'warning',
'processing' => 'primary',
'accepted' => 'success',
'rejected' => 'danger',
'closed' => 'secondary'
];

@endphp

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DAFTAR FAKTUR</h6>
    </div>
	<div class="card-body">
		<div class="table table-responsive">
			<table class="table table-sm table-bordered table-striped text-center text-nowrap">
				<thead class="thead-light">
					<tr>
						<th scope="col" class="text-center text-uppercase" width="5%">No.</th>
						<th scope="col" class="text-center text-uppercase">Faktur</th>
						<th scope="col" class="text-center text-uppercase">Produk</th>
						<th scope="col" class="text-center text-uppercase">SMS</th>
						<th scope="col" class="text-center text-uppercase">WhatsApp</th>
						<th scope="col" class="text-center text-uppercase">Harga</th>
						<th scope="col" class="text-center text-uppercase">Tanggal</th>
						<th scope="col" class="text-center text-uppercase">Status</th>
						<th scope="col" class="text-center text-uppercase">Update</th>
					</tr>
				</thead>
				<tbody>

					@forelse ($user->invoices as $invoice)

						<tr scope="row">
							@forelse ($invoice->products as $product)
							<th scope="col">{{ $number++ }}</th>
							<td scope="col" class="text text-monospace"><a href="{{ route('dashboard.invoice.show', ['invoice' => $invoice->ref]) }}">{{ $invoice->ref }}</a></td>							
							<td scope="col">{{ $product->name }}</td>
							<td class="text-center text-uppercase">{{ $product->sms }}</td>
							<td class="text-center text-uppercase">{{ $product->wa }}</td>
							<td scope="col">Rp. {{ number_format($invoice->price ?? 0, 0, ',', '.') }}</td>
							<td scope="col">{{ $invoice->created_at->format('d F Y') }}</td>
							<td scope="col">
								<span class="badge badge-{{ $colors[$invoice->status] }} shadow">{{ $invoice->status }}</span>
							</td>
							<td scope="col">{{ $invoice->updated_at->format('d F Y') }}</td>
							@empty
							@endforelse
						</tr>
					@empty
						<tr scope="row">
							<td colspan="9" class="text-center">Tidak ada data</td>
						</tr>
					@endforelse
					</tr>
				</tbody>
			</table>
		</div>
	</div>
    <div class="justify-content-center row">
        {!! $invoices->links('livewire.dashboard.pagination') !!}
    </div>
</div>
