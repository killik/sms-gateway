<div>
    @php

        $position = $user->id == $comment->user->id ? 'left' : 'right';

        $position2 = $user->id == $comment->user->id ? 'right' : 'left';

        $color = $user->id == $comment->user->id ? 'purple' : 'admin';

        $photo = $user->id == $comment->user->id ? asset('img/undraw_profile.svg') : asset('img/undraw_profile_2.svg');

    @endphp
    <div class="user-chat text-{{ $position }} float-{{ $position }}">
        <img class="img-profile rounded-circle" src="{{ $photo }}">
    </div>
    <div class="card text-{{ $position }} bg-{{ $color }} shadow h-100 pb-2 mb-2 mb-3"
        style="margin-{{ $position }}:60px;border-radius:4px;">
        <div class="meta-chat d-flex flex-row align-items-center justify-content-between">
            <div class="m-0">{{ $comment->user->name }}</div>
            <span class="float-{{ $position }}">{{ $comment->created_at->diffForHumans() }}</span>
        </div>

        <div class="card-body isi-chat">
            <div class="mb-0">{{ $comment->body }}</div>
        </div>
    </div>

</div>
