<div class="card shadow mb-4">
	<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
		<h6 class="m-0 text-primary">Live Support</h6>
	</div>
	<div class="card-body" style="overflow-y:scroll;max-height:340px;">
	@forelse ($comments as $comment)
        <livewire:dashboard.invoice.show.comment :comment="$comment" :key="$comment->id" :user="$user" />
    @empty
    @endforelse    
	</div>
	<div class="card-footer py-3">
		<form wire:submit.prevent="submit">
        <div class="form-group" style="margin-bottom:0">
            <div class="input input-group">
                <input type="text" class="form-control shadow @error('form') is-invalid @enderror"
                    placeholder="Ketik pesan" wire:model.debounce.500ms="form" />
                <div class="input-group-append shadow">
                    <button class="btn btn-outline-secondary" type="button" title="Refresh" wire:click="refresh">
                        <i class="fas fa-redo" wire:loading.class="fa-spin"></i>
                    </button>
                    <button class="btn btn-outline-secondary" type="submit">
                        <i class="fas fa-paper-plane"></i>
                        Kirim
                    </button>
                </div>
                @error('form')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>

                @enderror
            </div>
        </div>
    </form>
	</div>
	
</div>
