<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="payInvoiceModalDashboardInvoiceInputMessage">Bukti pembayaran</label>
        <input type="text" class="form-control @error('message') is-invalid @enderror" id="payInvoiceModalDashboardInvoiceInputMessage" wire:model.debounce.500ms="message">
        @error('message')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @else
       <span id="payInvoiceModalDashboardInvoiceInputMessageHelp" class="form-text text-muted">Invoice akan ditutup 7 x 24 Jam setelah dibuat.</span>
        @enderror
    </div>
	
    <div class="form-group custom-file mb-5">
        <label for="payInvoiceModalDashboardInvoiceShowSelectDocument" class="custom-file-label">Buktipembayaran</label>
        <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="payInvoiceModalDashboardInvoiceShowSelectDocument"wire:model="photo">
        @error('photo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @else
        <small id="payInvoiceModalDashboardInvoiceShowSelectDocumentHelp" class="form-text text-muted">Invoice akan ditutup 7 x 24 Jam setelah dibuat.            
			<div wire:loading wire:target="photo">Uploading...</div>
		</small>
        @enderror
    </div>

    @if ($photo)
    <div class="mt-4">
        <img class="img-fluid mb-3" src="{{ $photo->temporaryUrl() }}">
    </div>
    @endif
	
	
	<button type="submit" class="btn btn-primary btn-icon-split btn-sm">
        <span class="icon text-white-50">
            <i class="fas fa-upload" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-upload" style="padding-top:3px;"></i>
        </span>
        <span class="text">Konfirmasi</span>
     </button>
	 
	<button type="button" class="btn btn-danger btn-icon-split btn-sm pull-right" wire:click="resetForm">
        <span class="icon text-white-50">
            <i class="fas fa-times" style="padding-top:3px;padding-right:3px;"></i>
        </span>
        <span class="text">Reset</span>
    </button>
</form>
