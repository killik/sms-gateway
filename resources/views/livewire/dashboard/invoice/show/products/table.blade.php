@php

$number = 1;

$colors = [
    'pending' => 'warning',
    'processing' => 'primary',
    'accepted' => 'success',
    'rejected' => 'danger',
    'closed' => 'secondary',
];

@endphp


<div>
    <div class="table table-responsive">
        <table class="table text-nowrap table-borderless">
            <tbody>

                @forelse ($invoice->products as $product)

                    <tr scope="row">
                        <td scope="col" class="text-right"><strong>{{ $product->name }}</strong></td>
                        <td scope="col">Rp. {{ number_format($product->price ?? 0, 0, ',', '.') }}</td>
                    </tr>
                @empty
                    <tr scope="row">
                        <td colspan="3" class="text-center">Tidak ada data</td>
                    </tr>
                @else
                    <tr scope="row">
                        <td scope="col" class="text-right"><strong>Kode Unik Pembayaran</strong></td>
                        <td scope="col">Rp. {{ $invoice->code }}</td>
                    </tr>
                    <tr scope="row">
                        <td scope="col" class="text-right"><strong>Total</strong></td>
                        <td scope="col">Rp. {{ number_format($invoice->price ?? 0, 0, ',', '.') }}</td>
                    </tr>
                @endforelse
                </tr>
            </tbody>
        </table>
    </div>
</div>
