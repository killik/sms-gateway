<li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-envelope fa-fw"></i>
        <!-- Counter - Messages -->
        @if ($invoices->count() > 0)
            <span class="badge badge-danger badge-counter">{{ $invoices->count() }}</span>
        @endif
    </a>
    <!-- Dropdown - Messages -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
        aria-labelledby="messagesDropdown">
        <h6 class="dropdown-header">
            Message Center
        </h6>

        @forelse ($invoices as $item)
            @php
                $comments = $item->comments->reverse();

                $photo = ($user->id ?? 0) == ($comments->first()->user->id ?? 0) ? asset('img/undraw_profile_2.svg') : asset('img/undraw_profile.svg');
                if ($comments) {
                }
            @endphp
            <a wire:click="read({{ $item->id }})" class="dropdown-item d-flex align-items-center" type="button">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="{{ $photo }}" alt="">
                    <div class="status-indicator bg-success"></div>
                </div>
                <div class="font-weight-bold">
                    <div class="text-truncate">
                        {{ $comments->first()->body }}
                    </div>
                    <div class="small text-gray-500">{{ $comments->first()->user->name ?? 'User' }} ·
                        {{ $comments->first()->created_at->diffForHumans() }}</div>
                </div>
            </a>
        @empty

            <a class="dropdown-item text-center small text-gray-500" href="#">No items</a>

        @else

            <a class="dropdown-item text-center small text-gray-500" href="{{ route('admin.invoice.index') }}">Lihat
                semua chat</a>

        @endforelse
    </div>
</li>
