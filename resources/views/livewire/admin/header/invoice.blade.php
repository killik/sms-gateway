<li class="nav-item dropdown no-arrow show">
    <a class="nav-link dropdown-toggle" href="#"  id="invoiceDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-dollar-sign fa-fw"></i>
        <!-- Counter - Alerts -->
        @if ($pending + $processing > 0)
            <span class="badge badge-danger badge-counter">{{ $pending + $processing }}</span>
        @endif
    </a>
    <!-- Dropdown - User Information -->
    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="invoiceDropdown">
        <a class="dropdown-item" href="{{ route('admin.invoice.index', ['status' => 'pending']) }}">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            Pending

            @if ($pending + $processing > 0)
                <span class="badge badge-danger badge-counter">{{ $pending }}</span>
            @endif
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route('admin.invoice.index', ['status' => 'processing']) }}">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            Processing

            @if ($pending + $processing > 0)
                <span class="badge badge-danger badge-counter">{{ $processing }}</span>
            @endif
        </a>
    </div>
</li>
