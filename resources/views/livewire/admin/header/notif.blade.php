<li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-bell fa-fw"></i>
        <!-- Counter - Alerts -->
        @if ($notifs->count() > 0)
            <span class="badge badge-danger badge-counter">{{ $notifs->count() }}</span>
        @endif
    </a>
    <!-- Dropdown - Alerts -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
        aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header">
            Alerts Center
        </h6>
        @forelse ($notifs as $item)

            <a wire:click="read('{{ $item->id }}')" class="dropdown-item d-flex align-items-center" type="button">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="img/undraw_profile_1.svg" alt="">
                    <div class="status-indicator bg-success"></div>
                </div>
                <div class="font-weight-bold">
                    <div class="text-truncate">
                        {{ $item->data['title'] }}
                    </div>
                    <div class="small text-gray-500">
                        {{ $item->created_at->diffForHumans() }}
                    </div>
                </div>
            </a>
        @empty

            <a class="dropdown-item text-center small text-gray-500" href="#">No items</a>

        @endforelse
    </div>
</li>
