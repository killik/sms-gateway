<td class="drodown overflow-visible text-center">
    <a type="button" class="dropdown-toggle text-decoration-none" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <i class="fas fa-tools"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        @if ($product->active)

            <a class="dropdown-item dropdown" wire:click="inactive">
                Inactive
            </a>

        @else

            <a class="dropdown-item dropdown" wire:click="active">
                Active
            </a>

        @endif
        <a class="dropdown-item dropdown" wire:click="edit">
            Edit
        </a>
        <a class="dropdown-item dropdown" wire:click="delete">
            Delete
        </a>
    </div>
</td>
