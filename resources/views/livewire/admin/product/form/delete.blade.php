<form wire:submit.prevent="submit">
    <p>
        Anda yakin akan menghapus <b>{{ $name }}?
    </p>
    <button type="submit" class="btn btn-danger">
        <i class="fas fa-trash" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-trash"></i>
        Hapus
    </button>
    <button type="button" class="btn btn-secondary" wire:click="cancel">
        Batal
    </button>
</form>
