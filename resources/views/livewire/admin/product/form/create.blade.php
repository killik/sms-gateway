<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="{{ $modal }}InputName">
            Name
        </label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" id="{{ $modal }}InputName"
            aria-describedby="{{ $modal }}InputNamaHelp" placeholder="Masukan nama"
            wire:model.debounce.500ms="name">

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>
    <div class="form-group">
        <label for="{{ $modal }}InputDescription">
            Description
        </label>
        <textarea class="form-control @error('description') is-invalid @enderror"
            id="{{ $modal }}InputDescription" aria-describedby="{{ $modal }}InputDescriptionHelp"
            placeholder="Masukan deskripsi" wire:model.debounce.500ms="description">
        </textarea>
        @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>
    <div class="form-group">
        <label for="{{ $modal }}InputHarga">Harga</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
                <span class="input-group-text">{{ number_format($price, 0, ',', '.') }}</span>
            </div>
            <input type="number" class="form-control @error('price') is-invalid @enderror"
                id="{{ $modal }}InputHarga" aria-describedby="{{ $modal }}InputHargaHelp"
                placeholder="Masukan nominal" wire:model.debounce.500ms="price">
            @error('price')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="{{ $modal }}KuotaSMS">Kuota SMS</label>
                <input type="number" class="form-control @error('sms') is-invalid @enderror"
                    id="{{ $modal }}KuotaSMS" aria-describedby="{{ $modal }}KuotaSMSHelp"
                    placeholder="Masukan phone number" wire:model.debounce.500ms="sms">
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

            </div>
        </div>

        <div class="col">
            <div class="form-group">
                <label for="{{ $modal }}KuotaWA">Kuota Whatsapp</label>
                <input type="number" class="form-control @error('wa') is-invalid @enderror"
                    id="{{ $modal }}KuotaWA" aria-describedby="{{ $modal }}KuotaWAHelp"
                    placeholder="Masukan phone number" wire:model.debounce.500ms="wa">
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">
        <i class="fas fa-save" wire:loading.class="fa-redo fa-spin" wire:loading.class.remove="fa-save"></i>
        Simpan
    </button>
    <button type="button" class="btn btn-secondary" wire:click="resetForm">
        Reset
    </button>
</form>
