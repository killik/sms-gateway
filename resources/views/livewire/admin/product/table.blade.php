@php

$number = 1;

$colors = [
    'active' => 'primary',
    'inactive' => 'secondary',
];

@endphp

<a href="#" data-toggle="modal" data-target="#inputProductModalAdminProductIndex" class="btn btn-success btn-icon-split btn-sm" style="margin-bottom: 10px;">
    <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
    </span>
    <span class="text">Produk Baru</span>
</a>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Daftar Produk</h6>
    </div>

	<div class="card-body">
        <div class="table-responsive">
           <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
					<tr>
						<th class="text-center text-uppercase" width="2%">No.</th>
                        <th class="text-center text-uppercase">Nama Paket</th>
                        <th class="text-center text-uppercase">SMS</th>
                        <th class="text-center text-uppercase">WhatsApp</th>
                        <th class="text-center text-uppercase">Harga</th>
                        <th class="text-center text-uppercase">Terjual</th>
						<th class="text-center text-uppercase">Status</th>
						<th class="text-center text-uppercase">Dibuat</th>
						<th class="text-center text-uppercase">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($invoices as $product)
                    <tr>
                        <td class="text-center text-uppercase">{{ $number++ }}</td>
                        <td class="text-center">{{ $product->name }}</td>
                        <td class="text-center">{{ $product->sms }}</td>
                        <td class="text-center">{{ $product->wa }}</td>
                        <td class="text-center">Rp. {{ number_format($product->price, 0, ',', '.') }}</td>
                        <td class="text-center">{{ $product->hit }}</td>
                        <td class="text-center">{{ $product->active ? 'Active' : 'Inactive' }}</td>
                        <td class="text-center">{{ $product->created_at }}</td>

                        <livewire:admin.product.table.acction-td :product="$product" :key="$product->id" />
                    </tr>
                @empty
                    <tr scope="row">
                        <td colspan="9" class="text-center">Tidak ada data</td>
                    </tr>
                @endforelse
                </tr>
                </tbody>
             </table>
        </div>
    </div>
</div>
