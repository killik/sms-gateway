@php

$colors = [
    'pending' => 'warning',
    'processing' => 'primary',
    'accepted' => 'success',
    'rejected' => 'danger',
    'closed' => 'secondary',
];

@endphp

<div class="row">
    <div class="col-sm-12 col-md-6 col-xl-6">
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 mb-3">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body py-2">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs text-primary text-uppercase mb-1">
                                    Reference

                                    <a href="#" class="text-danger text-decoration-none float-right">

                                        <i class="fas fa-user"></i>

                                        User
                                    </a>
                                </div>
                                <div class="h5 mb-0 text-gray-800 text-monospace">
                                    {{ $invoice->ref }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 mb-3">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body py-2">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs text-success text-uppercase mb-1">
                                    Products
                                </div>
                                <div class="h5 mb-0 text-gray-800">

                                    {{ $invoice->products->count() }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 mb-3">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body py-2">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs text-danger text-uppercase mb-1">
                                    Harga
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="h5 mb-0 mr-3 text-gray-800">

                                            Rp. {{ number_format($invoice->price, 0, ',', '.') }}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-6 mb-3">
                <div class="card border-left-secondary shadow h-100 py-2">
                    <div class="card-body py-2">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs text-secondary text-uppercase mb-1">
                                    Status

                                    @if ($invoice->status != 'accepted')
                                        <span class="float-right">
                                            @if ($invoice->status != 'rejected')
                                                <a type="button" class="text-decoration-none  text-danger mr-2"
                                                    wire:click="reject">
                                                    <i class="fas fa-times"></i>
                                                    Tolak
                                                </a>
                                            @endif

                                            <a type="button" class="text-decoration-none text-primary"
                                                wire:click="accept">
                                                <i class="fas fa-check"></i>
                                                Terima
                                            </a>
                                        </span>
                                    @endif
                                </div>
                                <div class="h5 mb-0">

                                    <span class="badge badge-{{ $colors[$invoice->status] }} shadow">
                                        {{ $invoice->status }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if ($invoice->attachment)

                <!-- Pending Requests Card Example -->
                <div class="col-xl-12 mb-3">
                    <div class="card border-left-secondary shadow h-100 py-2">
                        <div class="card-body py-2">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs text-secondary text-uppercase mb-1">
                                        Bukti pembayaran

                                        @if (in_array($invoice->status, ['processing', 'rejected']))

                                            <span class="float-right">
                                                @if ($invoice->status != 'rejected')
                                                    <a type="button" class="text-decoration-none  text-danger mr-2"
                                                        wire:click="reject">
                                                        <i class="fas fa-times"></i>
                                                        Tolak
                                                    </a>
                                                @endif

                                                <a type="button" class="text-decoration-none text-primary"
                                                    wire:click="accept">
                                                    <i class="fas fa-check"></i>
                                                    Terima
                                                </a>
                                            </span>

                                        @endif

                                    </div>
                                    @if (!empty($invoice->attachment->description))

                                        <div class="mb-2">
                                            {{ $invoice->attachment->description }}
                                        </div>

                                    @endif
                                    <div class="mb-0">
                                        <a href="{{ asset('storage/invoice/' . $this->invoice->ref) }}"
                                            target="_blank">
                                            <img class="img-fluid"
                                                src="{{ asset('storage/invoice/' . $this->invoice->ref) }}"
                                                alt="{{ $invoice->ref }}" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endif

        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-xl-6">
        <livewire:dashboard.invoice.show.comments :invoice="$invoice" :user="$user" />
    </div>
</div>
