@php

$number = 1;

$colors = [

'pending' => 'warning',
'processing' => 'primary',
'accepted' => 'success',
'rejected' => 'danger',
'closed' => 'secondary'
];

@endphp

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DAFTAR INVOICE</h6>
    </div>
	<div class="card-body">
		<div class="table table-responsive">
			<table class="table table-sm table-bordered table-striped text-center text-nowrap">
				<thead class="thead-light">
					<tr>
						<th scope="col" class="text-center text-uppercase" width="5%">No.</th>
						<th scope="col" class="text-center text-uppercase">Invoice</th>
						<th scope="col" class="text-center text-uppercase">Pengguna</th>
						<th scope="col" class="text-center text-uppercase">Harga</th>
						<th scope="col" class="text-center text-uppercase">Jumlah Produk</th>
						<th scope="col" class="text-center text-uppercase">Tanggal</th>
						<th scope="col" class="text-center text-uppercase">Status</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($invoices as $invoice)
                    <tr scope="row">
                        <th scope="col">{{ $number++ }}</th>
                        <td scope="col" class="text text-monospace"><a
                                href="{{ route('admin.invoice.show', ['invoice' => $invoice->ref]) }}">{{ $invoice->ref }}</a>
                        </td>
                        <td scope="col" class="text text-monospace"><a href="#">{{ $invoice->user->email ?? NULL }}</a></td>
                        <td scope="col">Rp. {{ number_format($invoice->price, 0, ',', '.') }}</td>
                        <td scope="col">{{ $invoice->products->count() }}</td>
                        <td scope="col">{{ $invoice->created_at->format('d-m-Y') }}</td>						
                        <td scope="col">
                            <span class="badge badge-{{ $colors[$invoice->status] }} shadow">{{ $invoice->status }}</span>
                        </td>
                    </tr>
					@empty
                    <tr scope="row">
                        <td colspan="7" class="text-center">Tidak ada data</td>
                    </tr>
					@endforelse
					</tr>
				</tbody>
			</table>
		</div>
	</div>
    <div class="justify-content-center row">
        {!! $invoices->links('livewire.dashboard.pagination') !!}
    </div>
</div>
