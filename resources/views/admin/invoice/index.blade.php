@extends('layouts.admin')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:admin.invoice.table :status="$status" />

        <x-generic.lorem-ipsum :count="2" />

    </div>

@endsection
