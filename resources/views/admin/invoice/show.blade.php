@extends('layouts.admin')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:admin.invoice.show.row :invoice="$invoice" :user="$user" />

    </div>

@endsection
