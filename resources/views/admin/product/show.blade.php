@extends('layouts.admin')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:admin.product.show.row :product="$product" :user="$user" />

    </div>

@endsection
