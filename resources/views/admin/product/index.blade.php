@extends('layouts.admin')

@section('content')

    <x-bootstrap.modals.generic :title="'Input Product'" :id="'inputProductModalAdminProductIndex'">
        <livewire:admin.product.form.create :modal="'inputProductModalAdminProductIndex'" />
    </x-bootstrap.modals.generic>

    <x-bootstrap.modals.generic :title="'Delete Product'" :id="'deleteProductModalAdminProductIndex'">
        <livewire:admin.product.form.delete :modal="'deleteProductModalAdminProductIndex'" />
    </x-bootstrap.modals.generic>

    <x-bootstrap.modals.generic :title="'Update Product'" :id="'updateProductModalAdminProductIndex'">
        <livewire:admin.product.form.edit :modal="'updateProductModalAdminProductIndex'" />
    </x-bootstrap.modals.generic>

    <div class="container container-fluid pt-2 pb-4 mb-4">
        <livewire:admin.product.table :status="$status" />
    </div>

@endsection
