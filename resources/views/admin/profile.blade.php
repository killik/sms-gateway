@extends('layouts.admin')

@section('content')

    <!-- Modal -->
    <div class="modal fade" id="editProfileModalDashboardProfile" tabindex="-1"
        aria-labelledby="editProfileModalDashboardProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editProfileModalDashboardProfileLabel">Edit Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.profile.form.update :user="$user" :modal="'editProfileModalDashboardProfile'" />

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="cpassProfileModalDashboardProfile" tabindex="-1"
        aria-labelledby="cpassProfileModalDashboardProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cpassProfileModalDashboardProfileLabel">Ganti Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.profile.form.cpass :user="$user" :modal="'cpassProfileModalDashboardProfile'" />

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="quotaProfileModalDashboardProfile" tabindex="-1"
        aria-labelledby="quotaProfileModalDashboardProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="quotaProfileModalDashboardProfileLabel">Beli Quota</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.profile.form.quota :user="$user" :modal="'quotaProfileModalDashboardProfile'" />

                </div>
            </div>
        </div>
    </div>

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.profile.row :user="$user" />

        <livewire:dashboard.cards.token :user=$user />

        <x-generic.lorem-ipsum :count="4" />

    </div>

@endsection
