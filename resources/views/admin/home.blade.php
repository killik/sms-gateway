@extends('layouts.admin')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:admin.chart.sms :days="31" />

        <x-generic.lorem-ipsum :count="8" />

    </div>

@endsection
