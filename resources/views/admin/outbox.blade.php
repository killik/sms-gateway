@extends('layouts.admin')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.outbox.row :user="$user" :status="$status" />

        <x-generic.lorem-ipsum :count="1" />

    </div>

@endsection
