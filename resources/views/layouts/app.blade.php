<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="id" class="notranslate" translate="no">

<head>
	<?php setlocale(LC_ALL, 'id_ID.utf8'); ?>
    <meta charset="id_ID.utf8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ config('app.name', 'Laravel') }}</title>

	<link rel="shortcut icon" type="image/png" href="/favicon.ico" id="favicon" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center"
                href="{{ route('dashboard.home') }}">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">{{ config('app.name', 'Laravel') }}</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            @guest

                <li class="nav-item active">
                    @if (Route::has('login'))
                        <a class="nav-link" href="{{ route('login') }}" alt="{{ __('Login') }}"><i
                                class="bi bi-person-check-fill"></i></a>
                    @endif

                    @if (Route::has('register'))
                        <a class="nav-link" href="{{ route('register') }}" alt="{{ __('Register') }}"><i
                                class="bi bi-person-plus-fill"></i></a>
                    @endif
                </li>
            @else

                @can('access.admin')
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin.home') }}" alt="{{ __('Register') }}"><i
                                class="bi bi-speedometer"></i> <span>Admin Panel</span></a>
                    </li>
                @endcan




                <!-- Nav Item - User Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard.profile') }}"><i class="fas fa-fw fa-user"></i>
                        <span>Profil</span></a>
                </li>

                <!-- Nav Item - Produk -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard.product.index') }}"><i class="fas fa-fw fa-folder"></i>
                        <span>Produk</span></a>
                </li>

                <!-- Nav Item - Whatsapp -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWhatsapp"
                        aria-expanded="true" aria-controls="collapseWhatsapp">
                        <i class="bi bi-whatsapp"></i>
                        <span>Whatsapp</span>
                    </a>
                    <div id="collapseWhatsapp" class="collapse" aria-labelledby="headingWhatsapp"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item"
                                href="{{ route('dashboard.whatsapp.connect') }}">{{ __('Sambungan') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.whatsapp.send') }}">{{ __('Kirim Pesan') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.whatsapp.outbox.index') }}">{{ __('Kotak Keluar') }}</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Pesan Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                        aria-expanded="true" aria-controls="collapseUtilities">
                        <i class="bi bi-chat-square-dots-fill"></i>
                        <span>Pesan</span>
                    </a>
                    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item"
                                href="{{ route('dashboard.sms.inbox.index') }}">{{ __('Kotak Masuk') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.sms.outbox.index') }}">{{ __('Kotak Keluar') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.sms.outbox.index', ['status' => 'pending']) }}">{{ __('Tertunda') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.sms.outbox.index', ['status' => 'failed']) }}">{{ __('Gagal') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.sms.outbox.index', ['status' => 'queued']) }}">{{ __('Antrian') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.sms.outbox.index', ['status' => 'sent']) }}">{{ __('Terkirim') }}</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Kontak Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKontak"
                        aria-expanded="true" aria-controls="collapseKontak">
                        <i class="fas fa-id-card-alt"></i>
                        <span>Kontak</span>
                    </a>
                    <div id="collapseKontak" class="collapse" aria-labelledby="headingKontak"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item"
                                href="{{ route('dashboard.contact.contact.index') }}">{{ __('Daftar Kontak') }}</a>
                            <a class="collapse-item" onclick="alert('---')">{{ __('Grup Kontak') }}</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Faktur Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFaktur"
                        aria-expanded="true" aria-controls="collapseFaktur">
                        <i class="bi bi-award"></i>
                        <span>Faktur</span>
                    </a>
                    <div id="collapseFaktur" class="collapse" aria-labelledby="headingFaktur"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item"
                                href="{{ route('dashboard.invoice.index') }}">{{ __('Semua') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.invoice.index', ['status' => 'accepted']) }}">{{ __('Terbayar') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.invoice.index', ['status' => 'pending']) }}">{{ __('Terhutang') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.invoice.index', ['status' => 'processing']) }}">{{ __('Proses') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.invoice.index', ['status' => 'rejected']) }}">{{ __('Tolak') }}</a>
                            <a class="collapse-item"
                                href="{{ route('dashboard.invoice.index', ['status' => 'closed']) }}">{{ __('Tutup') }}</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

                <!-- Sidebar Message -->
                <div class="sidebar-card">
                    <img class="sidebar-card-illustration mb-2"
                        src="https://www.indomaxhost.com/wp-content/uploads/2019/01/logo-2-1.png" alt="www.indomaxhost.com"
                        style="width:100%;height:auto;">
                    <p class="text-center mb-2"><strong>Unlimited Hosting</strong><br /> Free
                        domain:<br />.com, .id, .my.id, .web.id</p>
                    <a class="btn btn-primary btn-sm" href="https://client.indomaxhost.com/cart.php?a=add&pid=7"
                        target="_blank">Sewa Hosting</a>
                </div>
            @endguest
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->

                    <ul class="navbar-nav ml-auto">
                        @guest
                            <li class="nav-item active">
                                @if (Route::has('login'))
                                    <a class="nav-link" href="{{ route('login') }}" alt="{{ __('Login') }}"><i
                                            class="bi bi-person-check-fill"></i></a>
                                @endif

                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}" alt="{{ __('Register') }}"><i
                                            class="bi bi-person-plus-fill"></i></a>
                                @endif
                            </li>
                        @else

                            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                            <li class="nav-item dropdown no-arrow d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-search fa-fw"></i>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                    aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto w-100 navbar-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control bg-light border-0 small"
                                                placeholder="Search for..." aria-label="Search"
                                                aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fas fa-search fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>


                            <livewire:dashboard.header.notif :user="$user" />

                            <livewire:dashboard.header.invoice :user="$user" />

                            <livewire:dashboard.header.message :user="$user" />


                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->

                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span
                                        class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                                    <img class="img-profile rounded-circle" src="{{ asset('img/undraw_profile.svg') }}">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                    aria-labelledby="userDropdown">
									@can('access.admin')
									<a class="dropdown-item" href="{{ route('admin.home') }}">
                                        <i class="fas fa-tachometer-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Admin Panel
                                    </a>
									<div class="dropdown-divider"></div>
									@endcan
                                    <a class="dropdown-item" href="{{ route('dashboard.profile') }}">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profil
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                        data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                @yield('content')
                <!-- /.container-fluid -->
                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>&copy; Portal SMS 2021</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->
            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->



    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up" style="padding-top:10px;font-size:23px;"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <livewire:scripts />

    @stack('scripts')

    <script>
        window.livewire.on('bootstrapModalShow', modal => $('#' + modal).modal('show'));
        window.livewire.on('bootstrapModalHide', modal => $('#' + modal).modal('hide'));

    </script>
</body>

</html>
