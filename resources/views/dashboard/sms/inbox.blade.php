@extends('layouts.app')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.inbox.row :user="$user" />

    </div>

@endsection
