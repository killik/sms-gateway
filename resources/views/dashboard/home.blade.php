@extends('layouts.app')

@section('content')

    <div class="container-fluid pt-2 pb-4 mb-4">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="{{ route('dashboard.product.index') }}" class="btn btn-primary btn-icon-split btn-sm">
                <span class="icon text-white-50">
                    <i class="fas fa-shopping-bag" style="padding-top:3px;"></i>
                </span>
                <span class="text">Top Up SMS</span>
            </a>
        </div>

        <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-2">
                <div class="info-box bg-midnight-bloom shadow">
                    <span class="info-box-icon"><i class="bi bi-download"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Sisa Kuota WA</span>
                        <span class="info-box-number">{{ $user->wa }}</span>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-2">
                <div class="info-box bg-grow-early shadow">
                    <span class="info-box-icon"><i class="bi bi-clock-history"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Sisa Kuota SMS</span>
                        <span class="info-box-number">{{ $user->sms }}</span>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-2">
                <div class="info-box bg-sunset shadow">
                    <span class="info-box-icon"><i class="bi bi-shield-slash"></i></span>
                    <div class="info-box-content">
                        <a class="info-box-text"
                            href="{{ route('dashboard.sms.outbox.index', ['status' => 'failed']) }}">Pesan Gagal</a>
                            <span class="info-box-number">{{ $user->outbox()->where('status', 'failed')->count() }}</span>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-2">
                <div class="info-box bg-arielle-smile shadow">
                    <span class="info-box-icon"><i class="bi bi-chat-square-text-fill"></i></span>
                    <div class="info-box-content">
                        <a class="info-box-text" href="{{ route('dashboard.sms.outbox.index') }}">Pesan Keluar</a>
                            <span class="info-box-number">{{ $user->outbox()->count() }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xl-8 xol-md-12">
                <livewire:dashboard.chart.sms :user="$user" :days="31" />

            </div>

            <div class="col-xl-4 col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Petunjuk Penggunaan Token Key SMS</h6>
                    </div>
                    <div class="card-body">
                        <ul style="margin-bottom:0;padding-inline-start: 0px;">
                            <ol>
                                <li>Copy <strong>Token Key SMS Gateway</strong> yang ada di halaman <strong><a
                                            href="{{ route('dashboard.profile') }}">Profil</a></strong>.</li>
                                <li>Buka halaman <strong>Administrator Portal Desa Digital</strong> anda dan login sebagai
                                    Administrator.</li>
                                <li>Masuk ke menu <strong>SMS > Pengaturan SMS</strong>.</li>
                                <li><strong>Paste</strong> Token Key SMS Gateway kedalam kolom Token Key SMS Gateway dan
                                    simpan.</li>
                            </ol>
                        </ul>
                    </div>
                </div>
				
				<div class="card shadow mb-4 bg-royal">
					<div class="card-body text-white">
						Layanan dari Portal SMS saat ini hanya dapat digunakan pada aplikasi <a href="https://www.portaldesadigital.id" target="_blank">Portal Desa Digital</a> dan saat ini hanya tersedia layanan sms saja. Untuk layanan WhatsApp dan Telegram sedang dalam pengembangan.
					</div>
				</div>
            </div>

        </div>

        {{-- <livewire:dashboard.chart.sms :user="$user" :days="31" /> --}}

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Syarat & Ketentuan</h6>
            </div>
            <div class="card-body">
                <ul style="margin-bottom:0;padding-inline-start: 0px;">
                    <ol>
                        <li>Setiap 1 SMS berisi 160 karakter. Jika dalam pengiriman SMS lebih dari 160 karakter maka
                            dihitung kelipatannya.</li>
                        <li>Pengguna dilarang menggunakan fasilitas ini untuk mengirim pesan
                            <ul>
                                <li class="text-danger"><strong>PENIPUAN</strong></li>
                                <li class="text-danger"><strong>PEMALSUAN</strong></li>
                                <li class="text-danger"><strong>FITNAH</strong></li>
                                <li class="text-danger"><strong>PERJUDIAN</strong></li>
                                <li class="text-danger"><strong>SARA</strong></li>
                                <li class="text-danger"><strong>PORNOGRAFI</strong></li>
                                <li class="text-danger"><strong>ANCAMAN</strong></li>
                                <li class="text-danger"><strong>UJARAN KEBENCIAN</strong></li>
                                <li class="text-danger"><strong>BLACK CAMPAIGN</strong></li>
                                <li class="text-danger"><strong>BERITA BOHONG/HOAX DAN SEGALA HAL YANG BERTENTANGAN DENGAN
                                        HUKUM YANG BERLAKU DI WILAYAH REPUBLIK INDONESIA</strong></li>
                            </ul>
                        </li>
                        <li>Pengguna bertanggung jawab sepenuhnya atas semua isi konten SMS/Pesan yang dikirim pengguna.
                        </li>
                        <li>Pengguna bertanggung jawab sepenuhnya atas segala dampak yang timbul yang diakibatkan dari isi
                            konten SMS/Pesan yang dikirim oleh pengguna.</li>
                        <li>Pengguna bertanggung jawab sepenuhnya dan tunduk sepenuhnya dengan aturan dan perundang-undangan
                            yang berlaku di wilayan Republik Indonesia.</li>
                        <li>Pengguna bertanggung jawab sepenuhnya dan tunduk sepenuhnya dengan aturan dan ketentuan yang
                            berlaku pada layanan PortalSMS.id.</li>
                        <li>PortalSMS.id tidak bertanggung jawab atas segala hal yang diakibatkan dari konten SMS/Pesan yang
                            dikirim oleh pengguna.</li>
                        <li>PortalSMS.id tidak bertanggung jawab atas diblokirnya suatu konten SMS/Pesan yang dikirim
                            pengguna oleh operator.</li>
                        <li>Harga sewaktu waktu dapat berubah, menyesuaikan dengan kebijakan operator selular.</li>
                    </ol>
                </ul>
            </div>
        </div>

    </div>
    </div>
    </div>

@endsection
