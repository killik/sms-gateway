@extends('layouts.app')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">
		
		<div class="text-center mb-4" style="padding:0 0 10px 0;border-bottom:1px solid #ddd;">
			<h3>Koneksi Whatsapp</h3>
		</div>

		<div class="row">
			<div class="col">
				<h3>Connection</h3>
				<button id="btn-logout" onclick="logout()" class="btn btn-danger">Logout</button>
				<img
					id="qr-image"
					style="display: none; width: 200px; height: 200px; border: 1px solid #f0f0f0;"
					alt="QR Code"
				/>
			</div>
			<div class="col">
				<h3>Logs</h3>
				<ul id="logs" style="background: #222; color: #fff; padding-left: 1.5em; max-height: 500px; overflow: auto"></ul>
			</div>
		</div>

		<script src="https://cdn.socket.io/4.4.1/socket.io.min.js"></script>
		<script>
			const $ = q => document.querySelector(q)
			const socket = io.connect('http://localhost:3000')

			socket.emit('start')
			
			socket.on('message', text => {
				const date = new Date
				const time = date.toLocaleString("id-ID", {timeZone: "Asia/Jakarta"});
				const item = document.createElement('li')
				item.textContent = `[${time.split(' ')[1].replace(/\./g, ':')}] ${text}`
				$('#logs').prepend(item)
			})

			socket.on('qr', url => {
				const img = $('#qr-image')
				img.src = url
				img.style.display = 'block'
			})

			socket.on('ready', () => {
				$('#btn-logout').style.display = 'inline-block'
			})

			function logout() {
				socket.emit('logout')
				setTimeout(() => {
					socket.emit('start')
				}, 1500)
			}
		</script>

    </div>

@endsection
