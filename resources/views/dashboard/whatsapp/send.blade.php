@extends('layouts.app')

@section('content')

    <div class="container">

        <livewire:dashboard.whatsapp.send :user="$user" />

    </div>

@endsection
