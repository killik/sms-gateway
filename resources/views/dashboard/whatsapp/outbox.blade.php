@extends('layouts.app')

@section('content')

    <div class="container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.outbox.row-whatsapp :user="$user" :status="$status" />

    </div>

@endsection
