<style>
.badge-success {
    color: #fff;
    background-color: #38c172;
}
</style>

@extends('layouts.print')

@section('content')

    <div class="container-fluid pt-2 pb-4 mb-4">

        @php

            $colors = [
                'pending' => 'warning',
                'processing' => 'primary',
                'accepted' => 'success',
                'rejected' => 'danger',
                'closed' => 'secondary',
            ];

        @endphp

        @php $num = 1; @endphp
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 text-primary"><strong>No. Faktur</strong> : {{ $invoice->ref }}</h6>
                <span class="pull-right"><strong>Tanggal Faktur</strong> :
                    {{ $invoice->created_at->isoFormat('D MMMM Y') }}</span>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h6 class="mb-3">Kepada</h6>
                        <strong class="text-uppercase" style="letter-spacing:1.8px;">{{ $user->name }}</strong>
                        <div>{{ $user->alamat }}</div>
                        <div>{{ $user->desa }}. {{ $user->kecamatan }}</div>
                        <div>{{ $user->kabupaten }}</div>
                        <div>Email : {{ $user->email }}</div>
                        <div>Telepon : {{ $user->phone }}</div>
                        <div class="mt-4"><strong>Status</strong> :
                            <span class="badge badge-{{ $colors[$invoice->status] }} shadow">
                                {{ $invoice->status }}
                            </span>
                            @if ($invoice->status == 'pending')
                                <!-- Button trigger modal -->
                                <a type="button" class="badge badge-danger" data-toggle="modal"
                                    data-target="#payInvoiceModalDashboardInvoiceShow">Konfirmasi Pembayaran</a>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <h6 class="mb-3">Dari</h6>
                        <strong class="text-uppercase"
                            style="letter-spacing:1.8px;">{{ config('app.name', 'Laravel') }}</strong>
                        <div>Jalan ABG, Gang Kerapu No.2</div>
                        <div>Kampung Melayu Bangsal. Ampenan Tengah</div>
                        <div>Kecamatan Ampenan. Kota Mataram</div>
                        <div>Lombok, Nusa Tenggara Barat</div>
                        <div>Telepon : 085239168707</div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th class="text-center text-uppercase">#</th>
                            <th class="text-center text-uppercase">Item</th>
                            <th class="text-center text-uppercase">Keterangan</th>
                            <th class="text-center text-uppercase">SMS</th>
                            <th class="text-center text-uppercase">WhatsApp</th>
                            <th class="text-right text-uppercase">Harga</th>
                        </tr>
                        <tr>
                            @forelse ($invoice->products as $product)
                                <td class="text-center text-uppercase">{{ $num++ }}</td>
                                <td class="text-center">
                                    <strong>{{ $product->name }}</strong>
                                </td>
                                <td class="text-center">
                                    {{ $product->description }}
                                </td>
                            @empty
                                <td colspan="6" class="text-center text-uppercase">Tidak Ada Data</t>
                                @else
                                <td class="text-center text-uppercase">{{ $product->sms }}</td>
                                <td class="text-center text-uppercase">{{ $product->wa }}</td>
                                <td class="text-right text-uppercase">Rp.
                                    {{ number_format($product->price ?? 0, 0, ',', '.') }}</td>
                            @endforelse
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h6 class="mt-3 mb-2"><strong style="border-bottom:1px solid">Rekening Pembayaran :</strong></h6>
                        <div>Bank BRI 349401005908506 Atas Nama : Happy Agung Pribadi</div>
                        <div>Bank Mandiri 1610002342967 Atas Nama : Happy Agung Pribadi</div>
                    </div>
                    <div class="col-md-4">
                        <table class="table table-clear">
                            <tbody>
                                <tr>
                                    <td class="text-left"><strong>Sub Total</strong></td>
                                    <td class="text-right text-uppercase">Rp.
                                        {{ number_format($invoice->products->first()->price ?? 0, 0, ',', '.') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><strong>Kode Unik</strong></td>
                                    <td class="text-right text-uppercase">Rp. {{ $invoice->code }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><strong>Total</td>
                                    <td class="text-right text-uppercase"><strong>Rp.
                                            {{ number_format($invoice->price ?? 0, 0, ',', '.') }}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @if ($invoice->status == 'accepted')
                    <div class="row mt-4">
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                            <div class="text-center">Mataram, {{ $invoice->updated_at->isoFormat('D MMMM Y') }}</div>
                            <div class="mt-4 mb-4">&nbsp;</div>
                            <div class="text-center mb-4"
                                style="font-weight:700;border-top:1px solid #444;padding-top:3px;">
                                Happy Agung Pribadi</div>
                        </div>
                    </div>
                @endif
            </div>
        </div>


    </div>

@endsection
