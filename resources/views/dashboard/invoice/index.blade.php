@extends('layouts.app')

@section('content')

    <div class="container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.invoice.table :status="$status" :user="$user" />

        <x-generic.lorem-ipsum :count="2" />

    </div>

@endsection
