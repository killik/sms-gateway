@extends('layouts.app')

@section('content')

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.contact.group.table :user="$user" />

        <x-generic.lorem-ipsum :count="2" />

    </div>

@endsection
