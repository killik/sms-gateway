@extends('layouts.app')

@section('content')

    <!-- Modal -->
    <div class="modal fade" id="payInvoiceModalDashboardInvoiceShow" tabindex="-1"
        aria-labelledby="payInvoiceModalDashboardInvoiceShowLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="payInvoiceModalDashboardInvoiceShowLabel">Edit Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.invoice.show.pay :invoice="$invoice"
                        :modal="'payInvoiceModalDashboardInvoiceShow'" />

                </div>
            </div>
        </div>
    </div>

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.invoice.show.row :invoice="$invoice" :user="$user" />

    </div>

@endsection
