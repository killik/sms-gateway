@extends('layouts.app')

@section('content')

    <x-bootstrap.modals.generic :title="'Create Contact'" :id="'createContactModalDashboardContactContact'">
        <livewire:dashboard.contact.contact.form.create :user="$user"
            :modal="'createContactModalDashboardContactContact'" />
    </x-bootstrap.modals.generic>

    <x-bootstrap.modals.generic :title="'Import Contact'" :id="'importContactModalDashboardContactContactImport'">
        <livewire:dashboard.contact.contact.form.import :user="$user"
            :modal="'importContactModalDashboardContactContactImport'" />
    </x-bootstrap.modals.generic>

    <x-bootstrap.modals.generic :title="'Send Message'" :id="'smsContactModalDashboardContactContact'">
        <livewire:dashboard.contact.contact.form.sms :user="$user" :modal="'smsContactModalDashboardContactContact'" />
    </x-bootstrap.modals.generic>

    <x-bootstrap.modals.generic :title="'Delete Contact'" :id="'deleteContactModalDashboardContactContact'">
        <livewire:dashboard.contact.contact.form.delete :user="$user"
            :modal="'deleteContactModalDashboardContactContact'" />
    </x-bootstrap.modals.generic>

    <x-bootstrap.modals.generic :title="'Edit Contact'" :id="'editContactModalDashboardContactContact'">
        <livewire:dashboard.contact.contact.form.edit :user="$user" :modal="'editContactModalDashboardContactContact'" />
    </x-bootstrap.modals.generic>

    <div class="container container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.contact.contact.table :user="$user" />

        <x-generic.lorem-ipsum :count="1" />

    </div>

@endsection
