@extends('layouts.app')

@section('content')

    <!-- Modal -->
    <div class="modal fade" id="editProfileModalDashboardProfile" tabindex="-1"
        aria-labelledby="editProfileModalDashboardProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editProfileModalDashboardProfileLabel">Edit Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.profile.form.update :user="$user" :modal="'editProfileModalDashboardProfile'" />

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="cpassProfileModalDashboardProfile" tabindex="-1"
        aria-labelledby="cpassProfileModalDashboardProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cpassProfileModalDashboardProfileLabel">Ganti Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.profile.form.cpass :user="$user" :modal="'cpassProfileModalDashboardProfile'" />

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="quotaProfileModalDashboardProfile" tabindex="-1"
        aria-labelledby="quotaProfileModalDashboardProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="quotaProfileModalDashboardProfileLabel">Top Up Kuota</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <livewire:dashboard.profile.form.quota :user="$user" :modal="'quotaProfileModalDashboardProfile'" />

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-2 pb-4 mb-4">

        <livewire:dashboard.profile.row :user="$user" />

        <livewire:dashboard.cards.token :user=$user />

        <p>Layanan dari Portal SMS saat ini hanya dapat digunakan pada aplikasi <a href="https://www.portaldesadigital.id" target="_blank">Portal Desa Digital</a> dan saat ini hanya tersedia layanan sms saja. Untuk layanan WhatsApp dan Telegram sedang dalam pengembangan.</p>
		
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Petunjuk Penggunaan Token Key SMS Gateway</h6>
			</div>
			<div class="card-body">
				<ul style="margin-bottom:0;padding-inline-start: 0px;">
					<ol>
						<li>Copy <strong>Token Key SMS Gateway</strong> yang ada diatas.</li>
						<li>Buka halaman <strong>Administrator Portal Desa Digital</strong> anda dan login sebagai Administrator.</li>
						<li>Masuk ke menu <strong>SMS > Pengaturan SMS</strong>.</li>
						<li><strong>Paste</strong> Token Key SMS Gateway kedalam kolom Token Key SMS Gateway dan simpan.</li>
					</ol>
				</ul>
			</div>
		</div>

    </div>

@endsection
