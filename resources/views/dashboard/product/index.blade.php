@extends('layouts.app')

@section('content')

    <x-bootstrap.modals.generic :title="'Beli Paket'" :id="'productBuyModalDashboardProducts'">
        <livewire:dashboard.product.form.buy :user="$user" :modal="'productBuyModalDashboardProducts'" />
    </x-bootstrap.modals.generic>

    <div class="container container-fluid pt-2 pb-4 mb-4">
		
		<div class="text-center mb-4" style="padding:0 0 10px 0;border-bottom:1px solid #ddd;">
			<h3>PRODUK</h3>
		</div>

        <livewire:dashboard.product.table />

        <div class="alert alert-success">
			<ul style="margin-bottom:0;padding-inline-start:15px;">
				<li>Paket yang dibeli akan langsung aktif setelah pembayaran diterima</li>
				<li>Cara pembayaran bisa dilihat pada sms yang akan diterima oleh pengguna</li>
				<li>Segera lakukan pembayaran ke :
					<ul>
						<li>Bank BRI <strong>349401005908506</strong> Atas Nama : <strong>Happy Agung Pribadi</strong></li>
						<li>Bank Mandiri <strong>1610002342967</strong> Atas Nama : <strong>Happy Agung Pribadi</strong></li>
					</ul>
				</li>
				<li>Setelah melakukan pembayaran silakan upload bukti pembayaran dan konfirmasi ke Customer Service di WhatsApp : <a href="https://wa.me/085239168707" target="_blank"><strong>085239168707</strong></a></li>
			</ul>
		</div>

    </div>

@endsection
