Hai, {{ $user->name }}

Tagihan dengan dengan nomor faktur {{ $invoice->ref }} senilai Rp. {{ number_format($invoice->price, 0, ',', '.') }} telah dibuat, batas pembayaran sampai dengan {{ $invoice->created_at->addDays(7)->format('d-m-Y') }}

Silakan lakunan transfer pembayaran ke :
Bank BRI 349401005908506 Atas Nama : Happy Agung Pribadi
Bank Mandiri 1610002342967 Atas Nama : Happy Agung Pribadi

Setelah melakukan pembayaran segera upload bukti pembayaran http://portalsms.id/dashboard/invoice/{{ $invoice->ref }} atau konfirmasi pembayaran ke nomor WhatsApp +6285239168707

Salam
PortalSMS.id
