Hai, {{ $user->name }}

Tagihan dengan dengan nomor faktur {{ $invoice->ref }} senilai Rp. {{ number_format($invoice->price, 0, ',', '.') }} telah dibayar.

Segera periksa pembayaran http://portalsms.id/dashboard/invoice/{{ $invoice->ref }}

Salam
PortalSMS.id
