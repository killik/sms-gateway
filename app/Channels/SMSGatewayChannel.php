<?php

namespace App\Channels;

use App\Jobs\SMS\Notif;
use App\Models\Auth\User;
use Illuminate\Notifications\Notification;
use RuntimeException;

class SMSGatewayChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     */
    public function send(User $notifiable, Notification $notification)
    {
        $data = $this->getData($notifiable, $notification);
        Notif::dispatch($data['destination'], $data['body'])->onQueue('generic');
    }

    /**
     * Get the data for the notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array
     *
     * @throws \RuntimeException
     */
    protected function getData($notifiable, Notification $notification)
    {
        if (method_exists($notification, 'toSMSGateway'))
        {
            return call_user_func([$notification, 'toSMSGateway'], $notifiable);
        }

        throw new RuntimeException('Notification is missing toSMSGateway method.');
    }
}
