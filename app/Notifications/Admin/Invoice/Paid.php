<?php

namespace App\Notifications\Admin\Invoice;

use App\Channels\SMSGatewayChannel;
use App\Models\Invoice\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use RuntimeException;

class Paid extends Notification
{
    use Queueable;

    protected Invoice $invoice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        unset($notifiable);

        return [SMSGatewayChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->invoice;
    }

    public function toDatabase()
    {
        return [

            'title'         => sprintf('Faktur %s dibayar', $this->invoice->ref),
            'destination'   => 'admin.invoice.show',
            'options'       => ['invoice' => $this->invoice->ref],
            'icon'          => ''
        ];
    }

    public function toSMSGateway($notifiable)
    {
        if (empty($notifiable->phone))
        {
            throw new RuntimeException('Notifiable is missing phone number.');
        }

        return [

            'destination'   => $notifiable->phone,
            'body'          => view('notif.sms.admin.invoice.paid', ['invoice' => $this->invoice, 'user' => $this->invoice->user])
        ];
    }
}
