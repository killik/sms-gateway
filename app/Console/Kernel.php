<?php

namespace App\Console;

use App\Jobs\SMS\Inbox\Sync as SMSInboxSync;
use App\Models\Generic\Job\Queued;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $inboxSyncSchedule = $schedule->call(function ()
        {
            if (Queued::where('queue', 'message:inbox:sync')->count() < 4)
            {
                foreach(range(1, 6) as $time) {

                    SMSInboxSync::dispatch()->onQueue('message:inbox:sync')->delay(now()->addSeconds(10 * $time));
                }
            }
        });

        $inboxSyncSchedule->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
