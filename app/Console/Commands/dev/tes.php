<?php

namespace App\Console\Commands\dev;

use App\Services\SMS\SMSGateway;
use Illuminate\Console\Command;
use SMSGatewayMe\Client\Model\Message;

class tes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:tes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tes code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(SMSGateway $gateway)
    {
        $messages = collect($gateway->searchMessages($query)->getResults())->map(fn (Message $message): array =>  [

            'gateway_id'    => $message->getId(),
            'message'       => $message->getMessage(),
            'sender'        => $message->getPhoneNumber(),
            'received_at'   => now(),
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        print $messages->count();
    }
}
