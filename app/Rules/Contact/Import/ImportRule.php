<?php

namespace App\Rules\Contact\Import;

use App\Services\Contact\Import;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

class ImportRule implements Rule
{
    protected string $value;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  UploadedFile  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value->isValid() && Import::parse($value->get())->isValid())
        {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.regex');
    }
}
