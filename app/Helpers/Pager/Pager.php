<?php

namespace App\Helpers\Pager;

class Pager
{
    public static function make(int $total, int $page, int $limit): array
	{
		$current	= $page 	< 1 ? 1 : $page;
		$limit		= $limit	< 1 ? 1 : $limit;

		$last	= max((int) ceil($total / $limit), 1);

		$offset	= $current * $limit - $limit;

		return array_map(fn ($data) => intval($data), compact('current', 'last', 'limit', 'offset', 'total'));
	}
}
