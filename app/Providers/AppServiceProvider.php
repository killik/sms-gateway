<?php

namespace App\Providers;

use App\Services\SMS\SMSGateway;
// use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSMSGatewayMe();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Paginator::useBootstrap();
    }

    protected function registerSMSGatewayMe(): void
    {
        $config = $this->app['config']->get('smsgatewayme', [

            'key'       => '',
            'deviceIDs'  => []
        ]);

        $this->app->bindIf(SMSGateway::class, fn () => new SMSGateway(strval($config['key']), collect($config['deviceIDs'])));
    }
}
