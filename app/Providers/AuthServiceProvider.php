<?php

namespace App\Providers;

use App\Models\Auth\User;
use App\Models\Contact\Contact;
use App\Models\Invoice\Invoice;
use App\Policies\Contact\ContactPolicy;
use App\Policies\Invoice\InvoicePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',

        Invoice::class  => InvoicePolicy::class,
        Contact::class  => ContactPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::after(fn (User $user) => $user->hasRole('super'));
    }
}
