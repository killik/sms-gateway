<?php

namespace App\Jobs\Generic\Notification;

use App\Models\Auth\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class NotifySuperUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Notification $notification;
    protected Collection $users;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification, Collection $users = null)
    {
        $this->notification = $notification;

        if (empty($users))
        {
            $this->users = User::whereHas('roles', fn ($roles) => $roles->where('name', 'super'))->get();
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->users->count() > 10)
        {
            $this->users->chunk(10)->each(function (Collection $users)
            {
                static::dispatch($this->notification, $users)->onQueue($this->job->queue);
            });
        }

        else
        {
            $this->users->each(fn (User $user) => $user->notify($this->notification));
        }
    }
}
