<?php

namespace App\Jobs\Contact\Import;

use App\Models\Auth\User;
use App\Models\Contact\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Insert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected User $user;
    protected Collection $contacts;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Collection $contacts)
    {
        $this->user = $user;
        $this->contacts = $contacts;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $contacts = $this->contacts->reject(fn (array $contact) => Contact::where('phone_number', $contact['number'])->exists());

        $data = $contacts->map(fn (array $contact) => [

            'name' => $contact['name'],
            'phone_number' => $contact['number'],
            'user_id' => $this->user->id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Contact::insert($data->all());
    }
}
