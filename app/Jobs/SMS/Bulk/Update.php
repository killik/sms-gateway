<?php

namespace App\Jobs\SMS\Bulk;

use App\Jobs\SMS\Outbox\ReloadCombo;
use App\Models\Message\Bulk;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SMSGatewayMe\Client\Model\Message;

class Update implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Bulk $bulk;
    protected Collection $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Bulk $bulk, Collection $messages)
    {
        $bulk->update(['status' => 'queued']);

        $this->bulk = $bulk;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->messages->each(function (Message $message)
        {
            $this->bulk->outbox()

            ->where('destination', $message->getPhoneNumber())
            ->orWhere('destination', Str::start($message->getPhoneNumber(), '0'))
            ->update([

                'status'        => $message->getStatus(),
                'gateway_id'    => $message->getId()
            ]);

            ReloadCombo::dispatch($this->bulk->outbox()->where('gateway_id', $message->getId())->first())->onQueue('message:reload1');
        });

        if ($this->bulk->outbox()->where('status', 'failed')->exists())
        {
            $this->bulk->update(['status' => 'failed']);
        }

        else
        {
            $this->bulk->update(['status' => 'sent']);
        }
    }
}
