<?php

namespace App\Jobs\SMS\Bulk;

use App\Models\Message\Bulk as MessageBulk;
use App\Models\Message\Outbox;
use App\Services\SMS\SMSGateway;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Bulk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected MessageBulk $bulk;
    protected Collection $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MessageBulk $bulk, Collection $messages)
    {
        $bulk->update(['status' => 'queued']);

        $this->bulk = $bulk;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSGateway $gateway)
    {
        $this->bulk->update(['status' => 'processing']);

        try
        {
            $messages = $this->messages->map(fn (Outbox $message) => [

                'destination'   => $message->destination,
                'body'          => $message->message,
            ]);

            $gateway->bulkMessage($messages)->chunk(25)->each(function (Collection $messages)
            {
                Update::dispatch($this->bulk, $messages)->onQueue('generic');
            });
        }

        catch (Exception $e)
        {
            $this->bulk->update(['status' => 'failed']);

            throw $e;
        }
    }
}
