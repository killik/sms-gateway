<?php

namespace App\Jobs\SMS\Inbox;

use App\Models\Message\Inbox;
use App\Services\SMS\SMSGateway;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SMSGatewayMe\Client\Model\Message;

class Sync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSGateway $gateway)
    {
        $lastMessage = Inbox::orderBy('gateway_id', 'desc')->first();

        $query = [

            'filters' => [

                [
                    [
                        'field'     => 'id',
                        'operator'  => '>',
                        'value'     => $lastMessage->gateway_id ?? 0
                    ],

                    [
                        'field'     => 'status',
                        'operator'  => '=',
                        'value'     => 'received'
                    ]
                ]
            ],

            'order_by' => [

                [
                    'field'     => 'id',
                    'direction' => 'asc'
                ]
            ]
        ];

        $messages = collect($gateway->searchMessages($query)->getResults())->map(fn (Message $message): array =>  [

            'gateway_id'    => $message->getId(),
            'message'       => $message->getMessage(),
            'sender'        => $message->getPhoneNumber(),
            'received_at'   => now(),
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        Inbox::insert($messages->all());
    }
}
