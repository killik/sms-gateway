<?php

namespace App\Jobs\SMS\Outbox;

use App\Models\Message\Outbox;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReloadCombo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Outbox $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Outbox $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(1));

        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(10));
        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(15));
        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(20));
        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(25));
        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(30));
        Reload::dispatch($this->message, true)->onQueue('message:reload1')->delay(now()->addMinutes(35));

        Reload::dispatch($this->message, true)->onQueue('message:reload2')->delay(now()->addHour());

        Reload::dispatch($this->message, true)->onQueue('message:reload2')->delay(now()->addDay());
    }
}
