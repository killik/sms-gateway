<?php

namespace App\Jobs\SMS\Outbox;

use App\Models\Message\Outbox;
use App\Services\SMS\SMSGateway;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Reload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Outbox    $message;
    protected String    $oldStatus;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Outbox $message, Bool $noStatusCange = false)
    {
        if (!$noStatusCange)
        {
            $message->update(['status' => 'queued']);
        }

        $this->oldStatus = $message->status;

        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSGateway $gateway)
    {
        $this->message->update(['status' => 'processing']);

        try
        {
            $message = $gateway->getMessage($this->message->gateway_id);

            $data = [

                'status'        => $message->getStatus(),
                'gateway_id'    => $message->getID()
            ];

            if ($message->getStatus() == 'sent' && empty($this->sent_at))
            {
                foreach ($message->getLog() as $log)
                {
                    if ($log->getStatus() == 'sent')
                    {
                        $data['sent_at'] = $log->getOccurredAt();
                    }
                }
            }

            $this->message->update($data);
        }
        catch (Exception $e)
        {
            $this->message->update(['status' => $this->oldStatus]);

            if (empty($this->message->gateway_id))
            {
                $this->message->update(['status' => 'failed']);
            }

            throw $e;
        }
    }

    public static function combo(Outbox $message): void
    {
        ReloadCombo::dispatch($message)->onQueue('message:reload2');
    }
}
