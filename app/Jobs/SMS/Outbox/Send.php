<?php

namespace App\Jobs\SMS\Outbox;

use App\Models\Message\Outbox;
use App\Services\SMS\SMSGateway;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class Send implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Outbox $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Outbox $message)
    {
        $message->update(['status' => 'queued']);

        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSGateway $gateway)
    {
        if ($this->message->status != 'processing')
        {
            $this->message->update(['status' => 'processing']);
        }

        try
        {
        
            Log::info('--------fffffffffffff-------');
            Log::info($this->message->destination);
            
            $message = $gateway->sendMessage($this->message->destination, $this->message->message);

            $this->message->update([

                'status'        => $message->getStatus(),
                'gateway_id'    => $message->getID()
            ]);

            Log::info('---------------');
            Log::info($this->message->destination);

            Reload::combo($this->message);
        }

        catch (Exception $e)
        {
            $this->message->update(['status' => 'failed']);

            throw $e;
        }
    }
}
