<?php

namespace App\Jobs\SMS;

use App\Services\SMS\SMSGateway;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Notif implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $destination, $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $destination, string $message)
    {
        $this->message = $message;
        $this->destination = $destination;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSGateway $gateway)
    {
        $gateway->sendMessage($this->destination, $this->message);
    }
}
