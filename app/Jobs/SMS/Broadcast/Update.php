<?php

namespace App\Jobs\SMS\Broadcast;

use App\Jobs\SMS\Outbox\ReloadCombo;
use App\Models\Message\Broadcast;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SMSGatewayMe\Client\Model\Message;

class Update implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Broadcast $broadcast;
    protected Collection $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Broadcast $broadcast, Collection $messages)
    {
        $broadcast->update(['status' => 'queued']);

        $this->broadcast = $broadcast;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->messages->each(function (Message $message)
        {
            $this->broadcast->outbox()

            ->where('destination', $message->getPhoneNumber())
            ->orWhere('destination', Str::start($message->getPhoneNumber(), '0'))
            ->update([

                'status'        => $message->getStatus(),
                'gateway_id'    => $message->getId()
            ]);

            ReloadCombo::dispatch($this->broadcast->outbox()->where('gateway_id', $message->getId())->first())->onQueue('message:reload2');
        });

        if ($this->broadcast->outbox()->where('status', 'failed')->exists())
        {
            $this->broadcast->update(['status' => 'failed']);
        }

        else
        {
            $this->broadcast->update(['status' => 'sent']);
        }
    }
}
