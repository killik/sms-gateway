<?php

namespace App\Jobs\SMS\Broadcast;

use App\Models\Message\Broadcast as MessageBroadcast;
use App\Models\Message\Outbox;
use App\Services\SMS\SMSGateway;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Broadcast implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected MessageBroadcast $broadcast;
    protected Collection $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MessageBroadcast $broadcast, Collection $messages)
    {
        $broadcast->update(['status' => 'queued']);

        $this->broadcast = $broadcast;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSGateway $gateway)
    {
        $this->broadcast->update(['status' => 'processing']);

        try
        {
            $destinations = $this->messages->map(fn (Outbox $message) => $message->destination);

            $gateway->broadcastMessage($destinations, $this->broadcast->message)->chunk(25)->each(function (Collection $messages)
            {
                Update::dispatch($this->broadcast, $messages)->onQueue('generic');
            });
        }

        catch (Exception $e)
        {
            $this->broadcast->update(['status' => 'failed']);

            throw $e;
        }
    }
}
