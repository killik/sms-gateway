<?php

namespace App\Jobs\Whatsapp\Bulk;

use App\Models\Whatsapp\Bulk as WhatsappBulk;
use App\Models\Whatsapp\Outbox;
use App\Services\Whatsapp\Message;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Bulk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected WhatsappBulk $bulk;
    protected Collection $messages;
    protected string $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(WhatsappBulk $bulk, Collection $messages, string $token)
    {
        $bulk->update(['status' => 'queued']);

        $this->bulk = $bulk;
        $this->messages = $messages;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $whatsapp = new Message($this->token);

        $this->bulk->update(['status' => 'processing']);

        try
        {
            $messages = $this->messages->map(fn (Outbox $message) => [

                'destination'   => $message->destination,
                'body'          => $message->message,
            ]);

            $whatsapp->bulkMessage($messages)->chunk(25)->each(function (Collection $messages)
            {
                Update::dispatch($this->bulk, $messages)->onQueue('generic');
            });
        }

        catch (Exception $e)
        {
            $this->bulk->update(['status' => 'failed']);

            throw $e;
        }
    }
}
