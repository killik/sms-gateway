<?php

namespace App\Jobs\Whatsapp\Bulk;

use App\Models\Whatsapp\Bulk;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Update implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Bulk $bulk;
    protected Collection $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Bulk $bulk, Collection $messages)
    {
        $bulk->update(['status' => 'queued']);

        $this->bulk = $bulk;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->messages->each(function (Array $message)
        {
            $this->bulk->outbox()

            ->where('destination', $message['destination'])
            ->orWhere('destination', Str::start($message['destination'], '0'))
            ->update([

                'status'        => 'sent',
                'gateway_id'    => time()
            ]);

        });

        if ($this->bulk->outbox()->where('status', 'failed')->exists())
        {
            $this->bulk->update(['status' => 'failed']);
        }

        else
        {
            $this->bulk->update(['status' => 'sent']);
        }
    }
}
