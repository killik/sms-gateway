<?php

namespace App\Jobs\Whatsapp\Broadcast;

use App\Models\Whatsapp\Broadcast as WhatsappBroadcast;
use App\Models\Whatsapp\Outbox;
use App\Services\Whatsapp\Message;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Broadcast implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected WhatsappBroadcast $broadcast;
    protected Collection $messages;
    protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(WhatsappBroadcast $broadcast, Collection $messages, string $token)
    {
        $broadcast->update(['status' => 'queued']);

        $this->token = $token;
        $this->broadcast = $broadcast;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $whatsapp = new Message($this->token);
        
        $this->broadcast->update(['status' => 'processing']);

        try
        {
            $destinations = $this->messages->map(fn (Outbox $message) => $message->destination);

            $whatsapp->broadcastMessage($destinations, $this->broadcast->message)->chunk(25)->each(function (Collection $messages)
            {
                Update::dispatch($this->broadcast, $messages)->onQueue('generic');
            });
        }

        catch (Exception $e)
        {
            $this->broadcast->update(['status' => 'failed']);

            throw $e;
        }
    }
}
