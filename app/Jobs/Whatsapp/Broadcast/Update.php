<?php

namespace App\Jobs\Whatsapp\Broadcast;

use App\Models\Whatsapp\Broadcast;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Update implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Broadcast $broadcast;
    protected Collection $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Broadcast $broadcast, Collection $messages)
    {
        $broadcast->update(['status' => 'queued']);

        $this->broadcast = $broadcast;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->messages->each(function (Array $message)
        {
            $this->broadcast->outbox()

            ->where('destination', $message['destination'])
            ->orWhere('destination', Str::start($message['destination'], '0'))
            ->update([

                'status'        => 'sent',
                'gateway_id'    => time()
            ]);

        });

        if ($this->broadcast->outbox()->where('status', 'failed')->exists())
        {
            $this->broadcast->update(['status' => 'failed']);
        }

        else
        {
            $this->broadcast->update(['status' => 'sent']);
        }
    }
}
