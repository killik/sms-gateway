<?php

namespace App\Jobs\Whatsapp\Outbox;

use App\Models\Whatsapp\Outbox;
use App\Services\Whatsapp\Message;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Send implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Outbox $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Outbox $message)
    {
        $message->update(['status' => 'queued']);

        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $whatsapp = new Message($this->message->user->api_token);

        if ($this->message->status != 'processing')
        {
            $this->message->update(['status' => 'processing']);
        }

        try
        {
        
            Log::info('--------fffffffffffff-------');
            Log::info($this->message->destination);
            
            $whatsapp->sendMessage($this->message->destination, $this->message->message);

            $this->message->update([

                'status'        => 'sent',
                'gateway_id'    => time()
            ]);

            Log::info('---------------');
            Log::info($this->message->destination);

        }

        catch (Exception $e)
        {
            $this->message->update(['status' => 'failed']);

            throw $e;
        }
    }
}
