<?php

namespace App\Jobs\Whatsapp\Outbox;

use App\Services\Whatsapp\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Notif implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $destination, $message, $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $destination, string $message, string $token)
    {
        $this->message = $message;
        $this->destination = $destination;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = new Message($this->token);
        $message->sendMessage($this->destination, $this->message);
    }
}
