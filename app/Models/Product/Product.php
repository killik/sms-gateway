<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product_products';

    protected $fillable = [
        'name',
        'description',
        'active',
        'price',
        'sms',
        'wa'
    ];
}
