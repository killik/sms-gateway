<?php

namespace App\Models\Invoice;

use App\Jobs\Generic\Notification\NotifySuperUsers;
use App\Jobs\Invoice\Close;
use App\Models\Auth\User;
use App\Models\Generic\Document;
use App\Models\Product\Product;
use App\Notifications\Admin\Invoice\Paid;
use App\Notifications\Dashboard\Invoice\Accepted;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [

        'status', 'ref'
    ];

    protected $table = 'invoice_invoices';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function acceptor(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function attachment(): BelongsTo
    {
        return $this->belongsTo(Document::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function detiles(): HasMany
    {
        return $this->hasMany(Detile::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'invoice_invoice_product');
    }

    public function process(): void
    {
        $this->update(['status' => 'processing']);

        NotifySuperUsers::dispatch(new Paid($this))->onQueue('generic');
    }

    public function reject(): self
    {
        $this->update(['status' => 'rejected']);

        Close::dispatch($this)->onQueue('generic')->delay(now()->addDays(7));

        return $this;
    }

    public function accept(User $acceptor): self
    {
        if (!in_array($this->status, ['accepted']))
        {
            $this->acceptor()->associate($acceptor);

            $this->user->increment('sms', $this->products()->first()->sms);
            $this->user->increment('wa', $this->products()->first()->wa);

            $this->update(['status' => 'accepted']);

            $this->user->notify(new Accepted($this));

            foreach ($this->products as $product)
            {
                $product->increment('hit');
            }

            Close::dispatch($this)->onQueue('generic')->delay(now()->addHours(6));
        }

        return $this;
    }

    public function close(): self
    {
        $this->update(['status' => 'closed']);

        return $this;
    }

    public function getCodeAttribute(): int
    {
        return intval(Str::substr($this->ref, 6));
    }

    public function getPriceAttribute(): int
    {
        return $this->products->sum('price') + $this->code;
    }

    public function makeRefCode(): self
    {
        $this->ref = Str::random(6) . mt_rand(100, 999);

        if (static::where('ref', $this->ref)->exists())
        {
            $this->makeRefCode();
        }

        return $this;
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->makeRefCode()->status = 'pending';
        });

        static::created(fn (self $model) => static::invoiceCreated($model));
    }

    protected static function invoiceCreated(self $invoice): void
    {
        Close::dispatch($invoice)->onQueue('generic')->delay(now()->addDays(7));
    }
}
