<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;

class Detile extends Model
{
    use HasFactory;

    protected $fillable = [

        'invoice_id', 'price', 'product'
    ];

    protected $table = 'invoice_detiles';
}
