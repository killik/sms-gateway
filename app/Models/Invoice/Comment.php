<?php

namespace App\Models\Invoice;

use App\Models\Auth\User;
use App\Models\Generic\Document;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [

        'body', 'user_id'
    ];

    protected $table    = 'invoice_comments';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function attachment(): BelongsTo
    {
        return $this->belongsTo(Document::class);
    }

    public function readers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'invoice_comment_reader');
    }
}
