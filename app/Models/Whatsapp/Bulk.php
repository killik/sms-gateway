<?php

namespace App\Models\Whatsapp;

use App\Casts\Collection as CastsCollection;
use App\Jobs\Whatsapp\Bulk\Bulk as WhatsappBulk;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class Bulk extends Model
{
    use HasFactory;

    protected $fillable = [

        'messages', 'status'
    ];

    protected $casts = [

        'messages' => CastsCollection::class
    ];

    protected $table = 'whatsapp_bulks';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function outbox(): HasMany
    {
        return $this->hasMany(Outbox::class);
    }

    public function send(): void
    {
        if ($this->status == 'pending')
        {
            $messages = $this->outbox;
        }

        else
        {
            $query = $this->outbox()->where('status', 'failed')->orWhere(function (Builder $query)
            {
                $query->where('status', 'pending');

                $query->whereNull('gateway_id');
            });

            $messages = $query->get();
        }

        $token = $this->user->api_token;

        $messages->chunk(200)->each(fn (Collection $items) => WhatsappBulk::dispatch($this, $items, $token)->onQueue('whatsapp:send'));
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function (self $model): void
        {
            $model->status = 'pending';
        });

        static::created(function (self $model): void
        {
            $model->user->decrement('wa', $model->messages->count());

            $messages = $model->messages->map(fn (array $message) => [

                'user_id'       => $model->user->id,
                'bulk_id'       => $model->id,
                'message'       => $message['message'],
                'destination'   => $message['destination'],
                'status'        => 'pending',
                'created_at'    => now()
            ]);

            if (Outbox::insert($messages->all())) $model->send();
        });
    }
}
