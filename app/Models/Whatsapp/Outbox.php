<?php

namespace App\Models\Whatsapp;

use App\Jobs\Whatsapp\Outbox\Notif;
use App\Jobs\Whatsapp\Outbox\Send;
use App\Models\Auth\User;
use App\Models\Contact\Contact;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Outbox extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

        'sent_at' => 'datetime',
    ];

    protected $fillable = [

        'destination', 'message', 'status', 'sent_at', 'gateway_id'
    ];

    protected $hidden = [

        'user_id', 'broadcast_id', 'gateway_id'
    ];

    protected $table = "whatsapp_outbox";

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function contact(): HasOne
    {
        return $this->hasOne(Contact::class, 'phone_number', 'destination');
    }

    public function send(): void
    {
        if ($this->status == 'pending' || $this->status == 'failed')
        {
            if ($this->user->wa > 0) {
                Send::dispatch($this)->onQueue('whatsapp:send');
            }
        }
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function (self $model): void
        {
            $model->status = 'pending';
        });

        static::created(function (self $model): void
        {
            $user = $model->user;

            if ($user->wa > 0) {
                $user->decrement('wa');
                $model->send();
            }
            else {
                if (in_array($user->wa, [300, 200, 150, 100, 50, 40, 30]) || $user->wa < 20)
                {
                    $notif_message = view('notif.sms.user.kuota-sms', compact('user'))->render();
                    $notif_message = str_replace(' sms ', ' Whatsapp ', $notif_message);
                    $notif_message = str_replace($user->sms, $user->wa, $notif_message);
                    if ($user->phone) {
                        Notif::dispatch($user->phone, $notif_message, $user->api_token)->onQueue('generic');
                    }
                }
            }
        });
    }
}
