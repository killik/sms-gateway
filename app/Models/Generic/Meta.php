<?php

namespace App\Models\Generic;

use App\Casts\Json;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;

class Meta extends Model
{
    use HasFactory;

    protected $casts = [

        'data' => Json::class
    ];

    protected $fillable = [

        'key', 'data'
    ];

    protected $table = 'generic_metas';
}
