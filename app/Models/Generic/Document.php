<?php

namespace App\Models\Generic;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Document extends Model
{
    use HasFactory;

    protected $fillable = [

        'name', 'type', 'path', 'description'
    ];

    protected $table = 'generic_documents';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
