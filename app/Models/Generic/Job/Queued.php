<?php

namespace App\Models\Generic\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;

class Queued extends Model
{
    use HasFactory;

    protected $table = 'queued_jobs';
}
