<?php

namespace App\Models\Auth;

use App\Models\Contact\Contact;
use App\Models\Contact\Group;
use App\Models\Generic\Document;
use App\Models\Invoice\Invoice;
use App\Models\Message\Broadcast;
use App\Models\Message\Bulk;
use App\Models\Message\Inbox;
use App\Models\Message\Outbox;
use App\Models\Whatsapp\Outbox as OutboxWhatsapp;
use App\Models\Whatsapp\Broadcast as BroadcastWhatsapp;
use App\Models\Whatsapp\Bulk as BulkWhatsapp;
use App\Models\User as ModelsUser;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends ModelsUser
{
    use HasPermissions, HasRoles;

    protected $fillable = [

        'name', 'email', 'password', 'sms'
    ];

    public function inbox(): HasManyThrough
    {
        // Untuk Sementara.
        return $this->hasManyThrough(Inbox::class, Contact::class, null, 'sender', null, 'phone_number');
    }

    public function outbox(): HasMany
    {
        return $this->hasMany(Outbox::class);
    }

    public function outbox_whatsapp(): HasMany
    {
        return $this->hasMany(OutboxWhatsapp::class);
    }

    public function broadcasts(): HasMany
    {
        return $this->hasMany(Broadcast::class);
    }

    public function broadcasts_whatsapp(): HasMany
    {
        return $this->hasMany(BroadcastWhatsapp::class);
    }

    public function bulks(): HasMany
    {
        return $this->hasMany(Bulk::class);
    }

    public function bulks_whatsapp(): HasMany
    {
        return $this->hasMany(BulkWhatsapp::class);
    }

    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class);
    }

    public function contacts(): HasMany
    {
        return $this->hasMany(Contact::class);
    }

    public function contactGroups(): HasMany
    {
        return $this->hasMany(Group::class);
    }

    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }
}
