<?php

namespace App\Models\Contact;

use App\Models\Auth\User;
use App\Models\Message\Inbox;
use App\Models\Message\Outbox;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Contact extends Model
{
    use HasFactory;

    protected $table = 'contact_contacts';

    protected $fillable = [

        'phone_number', 'name'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'contact_contact_group');
    }

    public function outbox(): HasMany
    {
        return $this->hasMany(Outbox::class, 'destination', 'phone_number');
    }

    public function inbox(): HasMany
    {
        return $this->hasMany(Inbox::class, 'sender', 'phone_number');
    }
}
