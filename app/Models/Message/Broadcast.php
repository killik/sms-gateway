<?php

namespace App\Models\Message;

use App\Casts\Collection as CastsCollection;
use App\Jobs\SMS\Broadcast\Broadcast as SMSBroadcast;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class Broadcast extends Model
{
    use HasFactory;

    protected $casts = [

        'destinations' => CastsCollection::class
    ];

    protected $fillable = [

        'message', 'destinations', 'status'
    ];

    protected $table = 'message_broadcasts';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function outbox(): HasMany
    {
        return $this->hasMany(Outbox::class);
    }

    public function send(): void
    {
        if ($this->status == 'pending')
        {
            $messages = $this->outbox;
        }

        else
        {
            $query = $this->outbox()->where('status', 'failed')->orWhere(function (Builder $query)
            {
                $query->where('status', 'pending');

                $query->whereNull('gateway_id');
            });

            $messages = $query->get();
        }

        $messages->chunk(200)->each(fn (Collection $items) => SMSBroadcast::dispatch($this, $items)->onQueue('message:send'));
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function (self $model): void
        {
            $model->status = 'pending';
        });

        static::created(function (self $model): void
        {
            $model->user->decrement('sms', $model->destinations->count());

            $messages = $model->destinations->map(fn ($destination) => [

                'user_id'       => $model->user->id,
                'broadcast_id'  => $model->id,
                'message'       => $model->message,
                'destination'   => $destination,
                'status'        => 'pending',
                'created_at'    => now()
            ]);

            if (Outbox::insert($messages->all())) $model->send();
        });
    }
}
