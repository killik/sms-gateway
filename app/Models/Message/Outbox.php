<?php

namespace App\Models\Message;

use App\Jobs\SMS\Notif;
use App\Jobs\SMS\Outbox\Reload;
use App\Jobs\SMS\Outbox\Send;
use App\Models\Auth\User;
use App\Models\Contact\Contact;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Log;

class Outbox extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

        'sent_at' => 'datetime',
    ];

    protected $fillable = [

        'destination', 'message', 'status', 'sent_at', 'gateway_id'
    ];

    protected $hidden = [

        'user_id', 'broadcast_id', 'gateway_id'
    ];

    protected $table = "message_outbox";

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function contact(): HasOne
    {
        return $this->hasOne(Contact::class, 'phone_number', 'destination');
    }

    public function reload(): void
    {
        if ($this->status != 'queued' && $this->status != 'processing')
        {
            Reload::dispatch($this)->onQueue('message:reload');
        }
    }

    public function send(): void
    {
        if ($this->status == 'pending' || $this->status == 'failed')
        {
            if ($this->user->sms > 0) {
                Send::dispatch($this)->onQueue('message:send');
            }
        }
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function (self $model): void
        {
            $model->status = 'pending';
        });

        static::created(function (self $model): void
        {
            $user = $model->user;

            if ($user->sms > 0) {
                $user->decrement('sms');
                $model->send();
            }
            else {
                if (in_array($user->sms, [300, 200, 150, 100, 50, 40, 30]) || $user->sms < 20)
                {
                    Notif::dispatch($user->phone, view('notif.sms.user.kuota-sms', compact('user')))->onQueue('generic');
                }
            }
        });
    }
}
