<?php

namespace App\Models\Message;

use App\Models\Contact\Contact;
use Illuminate\Database\Eloquent\Factories\HasFactory;
 use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Inbox extends Model
{
    use HasFactory;

    protected $table = 'message_inbox';

    protected $fillable = [

        'sender', 'message', 'recheived_at', 'gateway_id'
    ];

    public function contact(): HasOne
    {
        return $this->hasOne(Contact::class, 'phone_number', 'sender');
    }
}
