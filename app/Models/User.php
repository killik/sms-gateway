<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name',
        'phone',
        'alamat',
        'desa',
        'kecamatan',
        'kabupaten', 'kodepos', 'email', 'password',
    ];

    protected $table = 'auth_users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

        'password', 'remember_token', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

        'email_verified_at' => 'datetime',
    ];

    public function makeApiToken(): self
    {
        $this->api_token = Str::random(200);

        if (static::where('api_token', $this->api_token)->exists())
        {
            $this->makeApiToken();
        }

        return $this;
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->makeApiToken();
        });
    }
}
