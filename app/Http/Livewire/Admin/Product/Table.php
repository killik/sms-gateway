<?php

namespace App\Http\Livewire\Admin\Product;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public ?String  $status;

    public String $keyword = '';

    protected $paginationTheme = 'bootstrap';

    protected $listeners = [

        'productAdded' => '$refresh',
        'productDeleted' => '$refresh',
        'productUpdated' => '$refresh',
    ];

    public function mount(?string $status): void
    {
        $this->status = $status;
    }

    public function render()
    {
        $invoices = Product::orderByDesc('id');

        if (!empty($this->status))
        {
            $invoices->where('active', $this->status == 'active' ? true : false);
        }

        if (!empty($this->keyword))
        {
            $invoices->where(function (Builder $query)
            {
                $query->where('ref', 'LIKE', '%' . $this->keyword . '%');

                $query->orWhereHas('user', function (Builder $query)
                {

                    $query->where('name', 'LIKE', '%' . $this->keyword . '%');
                    $query->orWhere('email', 'LIKE', '%' . $this->keyword . '%');
                });
            });
        }

        $invoices = $invoices->paginate(8);

        return view('livewire.admin.product.table', compact('invoices'));
    }
}
