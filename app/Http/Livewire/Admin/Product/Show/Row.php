<?php

namespace App\Http\Livewire\Admin\Product\Show;

use App\Models\Auth\User;
use App\Models\Product\Product;
use Livewire\Component;

class Row extends Component
{
    public Product $product;
    public User $user;

    public function mouth(Product $product, User $user): void
    {
        $this->product = $product;
        $this->user = $user;
    }

    public function accept(): void
    {
        $this->product->accept($this->user);
    }

    public function reject(): void
    {
        $this->product->reject();
    }

    public function render()
    {
        return view('livewire.admin.product.show.row');
    }
}
