<?php

namespace App\Http\Livewire\Admin\Product\Table;

use App\Models\Product\Product;
use Livewire\Component;

class AcctionTd extends Component
{
    public Product $product;

    public function mounth(Product $product): void
    {
        $this->product = $product;
    }

    public function inactive(): void
    {
        $this->product->update(['active' => false]);

        $this->emit('productUpdated');
    }

    public function active(): void
    {
        $this->product->update(['active' => true]);

        $this->emit('productUpdated');
    }

    public function edit(): void
    {
        $this->emitTo('admin.product.form.edit', 'editProduct', $this->product->id);
    }

    public function delete(): void
    {
        $this->emitTo('admin.product.form.delete', 'deleteProduct', $this->product->id);
    }

    public function render()
    {
        return view('livewire.admin.product.table.acction-td');
    }
}
