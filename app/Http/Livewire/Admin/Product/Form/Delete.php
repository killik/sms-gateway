<?php

namespace App\Http\Livewire\Admin\Product\Form;

use App\Models\Product\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class Delete extends Component
{
    use AuthorizesRequests;

    public  String  $name, $modal;
    public Product  $product;

    protected $listeners = [

        'deleteProduct' => 'deleteProductEvenListner'
    ];

    public function deleteProductEvenListner(Product $product): void
    {
        $this->name         = $product->name;

        $this->product = $product;

        $this->emit('bootstrapModalShow', $this->modal);
    }

    public function mount(string $modal): void
    {
        $this->modal    = $modal;
    }

    public function cancel()
    {
        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->authorize('delete', $this->product);

        $this->product->delete();

        $this->emit('productDeleted');

        $this->emit('bootstrapModalHide', $this->modal);
    }
}
