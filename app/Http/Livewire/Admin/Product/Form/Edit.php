<?php

namespace App\Http\Livewire\Admin\Product\Form;

use App\Models\Product\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class Edit extends Component
{
    use AuthorizesRequests;

    public string $name, $description, $modal;
    public int $price, $sms, $wa;

    public Product  $product;

    protected function getRules()
    {
        return [

            'name'          => 'required|min:6',
            'description'   => 'nullable',
            'price'         => 'required|numeric|min:100',
            'sms'           => 'nullable|integer',
            'wa'            => 'nullable|integer'
        ];
    }

    protected function updatingPrice(&$value): void
    {
        $value = intval($value);
    }

    protected function updatingWa(&$value): void
    {
        $value = intval($value);
    }

    protected function updatingSms(&$value): void
    {
        $value = intval($value);
    }

    protected $listeners = [

        'editProduct' => 'editProductEvenListner'
    ];

    public function editProductEvenListner(Product $product): void
    {
        $this->name         = $product->name;
        $this->description  = $product->description;
        $this->price        = $product->price;
        $this->sms          = $product->sms;
        $this->wa           = $product->wa;

        $this->product = $product;

        $this->emit('bootstrapModalShow', $this->modal);
    }
    public function mount(string $modal): void
    {
        $this->modal    = $modal;
    }

    public function resetForm()
    {
        $this->name         = $this->product->name;
        $this->description  = $this->product->description;
        $this->price        = $this->product->price;
        $this->sms          = $this->product->sms;
        $this->wa           = $this->product->wa;

        $this->name         = '';
        $this->description  = '';
        $this->price        = 0;
        $this->sms          = 0;
        $this->wa           = 0;

        $this->resetErrorBag();
    }

    public function editd($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $this->product->update([

            'name'          => $this->name,
            'description'   => $this->description,
            'price'         => $this->price,
            'sms'           => $this->sms,
            'wa'            => $this->wa
        ]);

        $this->emit('productUpdated', $this->product->id);

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render()
    {
        return view('livewire.admin.product.form.edit');
    }
}
