<?php

namespace App\Http\Livewire\Admin\Product\Form;

use App\Models\Auth\User;
use App\Models\Product\Product;
use Livewire\Component;

class Create extends Component
{
    public string $name, $description, $modal;
    public int $price, $sms, $wa;
    public  User    $user;

    protected function getRules()
    {
        return [

            'name'          => 'required|min:6',
            'description'   => 'nullable',
            'price'         => 'required|numeric|min:100',
            'sms'           => 'nullable|integer',
            'wa'            => 'nullable|integer'
        ];
    }

    protected function updatingPrice(&$value): void
    {
        $value = intval($value);
    }

    protected function updatingWa(&$value): void
    {
        $value = intval($value);
    }

    protected function updatingSms(&$value): void
    {
        $value = intval($value);
    }

    public function mount(User $user, string $modal): void
    {
        $this->name         = '';
        $this->description  = '';
        $this->price        = 1000;
        $this->sms          = 0;
        $this->wa           = 0;

        $this->modal    = $modal;
        $this->user     = $user;
    }

    public function resetForm()
    {
        $this->name         = '';
        $this->description  = '';
        $this->price        = 0;
        $this->sms          = 0;
        $this->wa           = 0;

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $product = Product::create([

            'name'          => $this->name,
            'description'   => $this->description,
            'price'         => $this->price,
            'sms'           => $this->sms,
            'wa'            => $this->wa
        ]);

        $this->emit('productAdded', $product->id);

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render()
    {
        return view('livewire.admin.product.form.create');
    }
}
