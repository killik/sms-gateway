<?php

namespace App\Http\Livewire\Admin\Chart;

use App\Http\Livewire\Dashboard\Chart\Sms as ChartSms;
use App\Models\Message\Outbox;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Sms extends ChartSms
{
    public int $days;

    public function getData(): Collection
    {
        $data = collect(range(-abs($this->days), 0))->map(function (int $value)
        {
            $date = now()->addDays($value);

            return [

                'carbon'    => $date,
                'date'      => $date->format('Y-m-d'),
                'count'     => 0,
                'day'       => $date->isoFormat('dddd')
            ];
        });

        $selectDate     = DB::raw('DATE(created_at) as date');
        $selectCount    = DB::raw('count(*) as count');

        $outbox = Outbox::where('status', 'sent')->where('created_at', '>=', $data->map(fn ($data) => $data['date'])->all());

        $groupedCount = $outbox->select($selectDate, $selectCount)->groupBy('date')->groupBy('date')->get();

        return $data->map(function ($item) use ($groupedCount)
        {
            /**
             * @var Collection
             */
            $data = $groupedCount->where('date', $item['date'])->first();

            if ($data)
            {
                $data =  $data->first();
                $item['count'] = $data['count'];
            }

            return $item;
        });
    }
}
