<?php

namespace App\Http\Livewire\Admin\Invoice\Show;

use App\Models\Auth\User;
use App\Models\Invoice\Invoice;
use Livewire\Component;

class Row extends Component
{
    public Invoice $invoice;
    public User $user;

    public function mouth(Invoice $invoice, User $user): void
    {
        $this->invoice = $invoice;
        $this->user = $user;
    }

    public function accept(): void
    {
        $this->invoice->accept($this->user);
    }

    public function reject(): void
    {
        $this->invoice->reject();
    }

    public function render()
    {
        return view('livewire.admin.invoice.show.row');
    }
}
