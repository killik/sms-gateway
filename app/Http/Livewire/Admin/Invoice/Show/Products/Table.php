<?php

namespace App\Http\Livewire\Admin\Invoice\Show\Products;

use App\Models\Auth\User;
use App\Models\Invoice\Invoice;
use Livewire\Component;

class Table extends Component
{
    public Invoice $invoice;

    public function mount(Invoice $invoice): void
    {
        $this->invoice     = $invoice;
    }

    public function render()
    {
        return view('livewire.admin.invoice.show.products.table');
    }
}
