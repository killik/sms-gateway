<?php

namespace App\Http\Livewire\Admin\Invoice;

use App\Models\Invoice\Invoice;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public ?String  $status;

    public String $keyword = '';

    protected $paginationTheme = 'bootstrap';

    public function mount(?string $status): void
    {
        $this->status = $status;
    }

    public function render()
    {
        $invoices = Invoice::orderByDesc('id');

        if (!empty($this->status))
        {
            $invoices->where('status', $this->status);
        }

        if (!empty($this->keyword))
        {
            $invoices->where(function (Builder $query)
            {
                $query->where('ref', 'LIKE', '%' . $this->keyword . '%');

                $query->orWhereHas('user', function (Builder $query)
                {

                    $query->where('name', 'LIKE', '%' . $this->keyword . '%');
                    $query->orWhere('email', 'LIKE', '%' . $this->keyword . '%');
                });
            });
        }

        $invoices = $invoices->paginate(8);

        return view('livewire.admin.invoice.table', compact('invoices'));
    }
}
