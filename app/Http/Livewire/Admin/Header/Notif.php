<?php

namespace App\Http\Livewire\Admin\Header;

use App\Models\Auth\User;
use Livewire\Component;

class Notif extends Component
{
    public User $user;

    public function mount(User $user): void
    {
        $this->user = $user;
    }

    public function read(string $notif): void
    {
        $notif = $this->user->notifications()->where('id', $notif)->first();

        $notif->markAsRead();

        $this->redirectRoute($notif->data['destination'], $notif->data['options']);
    }

    public function render()
    {
        $notifs = $this->user->unreadNotifications;

        return view('livewire.admin.header.notif', compact('notifs'));
    }
}
