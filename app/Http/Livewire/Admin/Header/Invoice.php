<?php

namespace App\Http\Livewire\Admin\Header;

use App\Models\Invoice\Invoice as InvoiceInvoice;
use Livewire\Component;

class Invoice extends Component
{
    public function render()
    {
        $pending = InvoiceInvoice::where('status', 'pending')->count();
        $processing = InvoiceInvoice::where('status', 'processing')->count();

        return view('livewire.admin.header.invoice', compact('pending', 'processing'));
    }
}
