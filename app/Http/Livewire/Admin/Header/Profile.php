<?php

namespace App\Http\Livewire\Admin\Header;

use Livewire\Component;

class Profile extends Component
{
    public function render()
    {
        return view('livewire.admin.header.profile');
    }
}
