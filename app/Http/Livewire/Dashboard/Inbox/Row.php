<?php

namespace App\Http\Livewire\Dashboard\Inbox;

use App\Models\Auth\User;
use Livewire\Component;
use Livewire\WithPagination;

class Row extends Component
{
    use WithPagination;

    public ?String  $status;

    public User     $user;

    public String  $keyword = '';

    protected $paginationTheme = 'bootstrap';

    public function mouth(User $user, ?string $status)
    {
        $this->user     = $user;
        $this->status   = $status;
    }

    public function render()
    {
        $inbox = $this->user->inbox();

        if (!empty($this->status))
        {
            $inbox->where('status', $this->status);
        }

        if (!empty($this->keyword))
        {
            $inbox->where(function ($query)
            {

                $query->where('destination', 'LIKE', '%' . $this->keyword . '%');
                $query->orWhere('message', 'LIKE', '%' . $this->keyword . '%');

                $query->orWhereHas('contact', function ($query)
                {
                    $query->where('name', 'LIKE', '%' . $this->keyword . '%');
                });
            });
        }

        $inbox = $inbox->paginate(9);

        return view('livewire.dashboard.inbox.row', compact('inbox'));
    }
}
