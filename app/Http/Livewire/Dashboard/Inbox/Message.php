<?php

namespace App\Http\Livewire\Dashboard\Inbox;

use App\Models\Message\Inbox;
use Livewire\Component;

class Message extends Component
{
    public Inbox $message;

    public function mounth(Inbox $message): void
    {
        $this->message = $message;
    }

    public function reload(): void
    {
        $this->message->reload();
    }

    public function send(): void
    {
        $this->message->send();
    }

    public function render()
    {
        return view('livewire.dashboard.inbox.message');
    }
}
