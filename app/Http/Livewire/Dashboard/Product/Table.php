<?php

namespace App\Http\Livewire\Dashboard\Product;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public String $keyword = '';

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $products = Product::orderByDesc('id')->where('active', true);

        if (!empty($this->keyword))
        {
            $products->where(function (Builder $query)
            {
                $query->where('name', 'LIKE', '%' . $this->keyword . '%');
            });
        }

        $products = $products->paginate(8);

        return view('livewire.dashboard.product.table', compact('products'));
    }
}
