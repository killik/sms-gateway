<?php

namespace App\Http\Livewire\Dashboard\Product\Form;

use App\Jobs\Generic\Notification\NotifySuperUsers;
use App\Jobs\SMS\Notif;
use App\Models\Auth\User;
use App\Models\Product\Product;
use App\Notifications\Admin\Invoice\Created as InvoiceCreated;
use App\Notifications\Dashboard\Invoice\Created;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Livewire\Component;

class Buy extends Component
{
    use AuthorizesRequests;

    public  int     $selected;
    public  String  $modal;
    public  User    $user;
    public Product $product;
    public Collection $products;

    protected   array   $rules = [

        'selected' => 'required|exists:product_products,id',
    ];

    protected $listeners = [

        'buyProduct' => 'buyProductEvenListner'
    ];

    public function buyProductEvenListner(Product $product): void
    {
        $this->selected = $product->id;

        $this->emit('bootstrapModalShow', $this->modal);
    }

    public function getPriceProperty(): int
    {
        return $this->products->where('id', $this->selected)->first()->price ?? 0;
    }

    public function mount(User $user, string $modal): void
    {
        $this->products = Product::whereActive(true)->get();

        $this->selected = $this->product->id ?? 0;

        $this->modal    = $modal;
        $this->user = $user;
    }

    public function cancel()
    {
        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $invoice = $this->user->invoices()->create();
        $invoice->products()->attach($this->selected);

        $this->user->notify(new Created($invoice));

        NotifySuperUsers::dispatch(new InvoiceCreated($invoice))->onQueue('generic');

        $this->emit('bootstrapModalHide', $this->modal);

        $this->redirect(route('dashboard.invoice.index'));
    }
}
