<?php

namespace App\Http\Livewire\Dashboard\Product\Table;

use App\Models\Product\Product;
use Livewire\Component;

class AcctionTd extends Component
{
    public Product $product;

    public function mounth(Product $product): void
    {
        $this->product = $product;
    }

    public function buy(): void
    {
        $this->emitTo('dashboard.product.form.buy', 'buyProduct', $this->product->id);
    }

    public function render()
    {
        return view('livewire.dashboard.product.table.acction-td');
    }
}
