<?php

namespace App\Http\Livewire\Dashboard\Chart;

use App\Models\Auth\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Sms extends Component
{
    public User $user;
    public int $days;

    public function getData(): Collection
    {
        $data = collect(range(-abs($this->days), 0))->map(function (int $value)
        {
            $date = now()->addDays($value);

            return [

                'carbon'    => $date,
                'date'      => $date->format('Y-m-d'),
                'count'     => 0,
                'day'       => $date->isoFormat('dddd')
            ];
        });

        $selectDate     = DB::raw('DATE(created_at) as date');
        $selectCount    = DB::raw('count(*) as count');

        $outbox = $this->user->outbox()->where('status', 'sent')->where('created_at', '>=', $data->map(fn ($data) => $data['date'])->all());

        $groupedCount = $outbox->select($selectDate, $selectCount)->groupBy('date')->groupBy('date')->get();

        return $data->map(function ($item) use ($groupedCount)
        {
            $data = $groupedCount->where('date', $item['date'])->first();

            if ($data)
            {
                $item['count'] = $data['count'];
            }

            return $item;
        });
    }

    public function render()
    {
        $dataChart = $this->getData();

        return view('livewire.dashboard.chart.sms', compact('dataChart'));
    }
}
