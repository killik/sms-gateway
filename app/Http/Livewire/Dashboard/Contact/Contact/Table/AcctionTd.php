<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact\Table;

use App\Models\Contact\Contact;
use Livewire\Component;

class AcctionTd extends Component
{
    public Contact $contact;

    public function mounth(Contact $contact): void
    {
        $this->contact = $contact;
    }

    public function edit(): void
    {
        $this->emitTo('dashboard.contact.contact.form.edit', 'editContact', $this->contact->id);
    }

    public function sms(): void
    {
        $this->emitTo('dashboard.contact.contact.form.sms', 'smsContact', $this->contact->id);
    }

    public function delete(): void
    {
        $this->emitTo('dashboard.contact.contact.form.delete', 'deleteContact', $this->contact->id);
    }

    public function render()
    {
        return view('livewire.dashboard.contact.contact.table.acction-td');
    }
}
