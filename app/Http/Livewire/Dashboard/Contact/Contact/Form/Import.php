<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact\Form;

use App\Jobs\Contact\Import\Insert;
use App\Models\Auth\User;
use App\Rules\Contact\Import\ImportRule;
use App\Services\Contact\Import as ContactImport;
use Illuminate\Support\Collection;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class Import extends Component
{
    use WithFileUploads;

    public $data;

    public Collection $contacts;

    public  User $user;

    public String $modal;

    protected function getRules(): array
    {
        return [

            'data' => [...explode('|', 'required|file|mimes:txt|max:2048'), new ImportRule],
        ];
    }

    public function mounth(User $user, string $modal): void
    {
        $this->user = $user;
        $this->modal = $modal;
    }

    public function updated(string $field): void
    {
        $this->validateOnly($field);
    }

    public function updatedData(TemporaryUploadedFile $value): void
    {
        $contacts = ContactImport::parse($value->get());

        if ($contacts->isValid())
        {
            $this->contacts = $contacts->getLines();
        }

        else
        {
            unset($this->contacts);
        }
    }

    public function resetForm(): void
    {
        unset($this->data, $this->contacts);

        $this->resetErrorBag();
    }

    public function submit(): void
    {
        $this->validate();

        $this->contacts->chunk(20)->each(fn (Collection $contacts) => Insert::dispatch($this->user, $contacts)->onQueue('generic'));

        $this->emitTo('dashboard.contact.contact.table', 'contactAdded');

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render()
    {
        return view('livewire.dashboard.contact.contact.form.import');
    }
}
