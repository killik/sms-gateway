<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact\Form;

use App\Models\Auth\User;
use App\Models\Contact\Contact;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class Edit extends Component
{
    use AuthorizesRequests;

    public  String  $name, $phone_number, $modal;
    public  User    $user;
    public Contact  $contact;

    protected  function getRules()
    {
        $id = $this->contact->id ?? null;

        return [

            'name'          => 'required|min:6',
            'phone_number'  => 'required|numeric|unique:contact_contacts,phone_number,'.$id
        ];
    }

    protected $listeners = [

        'editContact' => 'editContactEvenListner'
    ];

    public function editContactEvenListner(Contact $contact): void
    {
        $this->name         = $contact->name;
        $this->phone_number = $contact->phone_number;

        $this->contact = $contact;

        $this->emit('bootstrapModalShow', $this->modal);
    }

    public function mount(User $user, string $modal): void
    {
        $this->modal    = $modal;
        $this->user     = $user;
    }

    public function resetForm()
    {
        $this->name         = $this->contact->name;
        $this->phone_number = $this->contact->phone_number;

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $this->authorize('update', $this->contact);

        $this->contact->update([

            'name'          => $this->name,
            'phone_number'  => $this->phone_number
        ]);

        $this->emit('contactUpdated', $this->contact->id);

        $this->emit('bootstrapModalHide', $this->modal);
    }
}
