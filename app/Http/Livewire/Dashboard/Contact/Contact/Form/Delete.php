<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact\Form;

use App\Models\Auth\User;
use App\Models\Contact\Contact;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class Delete extends Component
{
    use AuthorizesRequests;

    public  String  $name, $phoneNumber, $modal;
    public  User    $user;
    public Contact  $contact;

    protected $listeners = [

        'deleteContact' => 'deleteContactEvenListner'
    ];

    public function deleteContactEvenListner(Contact $contact): void
    {
        $this->name         = $contact->name;
        $this->phoneNumber = $contact->phone_number;

        $this->contact = $contact;

        $this->emit('bootstrapModalShow', $this->modal);
    }

    public function mount(User $user, string $modal): void
    {
        $this->modal    = $modal;
        $this->user     = $user;
    }

    public function cancel()
    {
        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->authorize('delete', $this->contact);

        $this->contact->delete();

        $this->emit('contactDeleted');

        $this->emit('bootstrapModalHide', $this->modal);
    }
}
