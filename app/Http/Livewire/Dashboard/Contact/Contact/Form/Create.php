<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact\Form;

use App\Models\Auth\User;
use Livewire\Component;

class Create extends Component
{
    public  String  $name, $phone_number, $modal;
    public  User    $user;

    protected function getRules()
    {
        return [

            'name'          => 'required|min:6',
            'phone_number'  => 'required|numeric|unique:contact_contacts'
        ];
    }

    public function mount(User $user, string $modal): void
    {
        $this->name         = '';
        $this->phone_number = '';

        $this->modal    = $modal;
        $this->user     = $user;
    }

    public function resetForm()
    {
        $this->name         = '';
        $this->phone_number = '';

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $contact = $this->user->contacts()->create([

            'name'          => $this->name,
            'phone_number'  => $this->phone_number
        ]);

        $this->emit('contactAdded', $contact->id);

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render()
    {
        return view('livewire.dashboard.contact.contact.form.create');
    }
}
