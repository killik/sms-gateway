<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact\Form;

use App\Models\Auth\User;
use App\Models\Contact\Contact;
use Livewire\Component;

class Sms extends Component
{
    public  String  $message, $destination, $modal;
    public  User    $user;
    public Contact  $contact;

    protected $listeners = [

        'smsContact' => 'smsContactEvenListner'
    ];

    public function smsContactEvenListner(Contact $contact): void
    {
        $this->destination = $contact->phone_number;

        $this->contact = $contact;

        $this->emit('bootstrapModalShow', $this->modal);
    }

    protected function getRules()
    {
        return [

            'message'       => 'required|string',
            'destination'   => 'required|numeric'
        ];
    }

    public function mount(User $user, string $modal): void
    {
        $this->message      = '';
        $this->destination  = '';

        $this->modal    = $modal;
        $this->user     = $user;
    }

    public function resetForm()
    {
        $this->message      = '';
        $this->destination  = $this->contact->phone_number;

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $outbox = $this->user->outbox()->create([

            'message'       => $this->message,
            'destination'   => $this->destination
        ]);

        $this->emit('outboxAdded', $outbox->id);

        $this->emit('bootstrapModalHide', $this->modal);
    }
    public function render()
    {
        return view('livewire.dashboard.contact.contact.form.sms');
    }
}
