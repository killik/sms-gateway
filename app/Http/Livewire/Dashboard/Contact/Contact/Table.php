<?php

namespace App\Http\Livewire\Dashboard\Contact\Contact;

use App\Models\Auth\User;
use App\Services\Contact\Export;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\StreamedResponse;

class Table extends Component
{
    use WithPagination;

    public User $user;

    protected $listeners = [

        'contactAdded' => '$refresh',
        'contactUpdated' => '$refresh',
        'contactDeleted' => '$refresh',
        'outboxAdded' => '$refresh'
    ];

    public function mounth(User $user): void
    {
        $this->user = $user;
    }

    public function export(): StreamedResponse
    {
        return response()->streamDownload(fn () => Export::print($this->user), 'contacts.txt');
    }

    public function render()
    {
        $contacts = $this->user->contacts()->orderByDesc('id')->paginate(8);

        return view('livewire.dashboard.contact.contact.table', compact('contacts'));
    }
}
