<?php

namespace App\Http\Livewire\Dashboard\Contact\Group\Form;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.dashboard.contact.group.form.create');
    }
}
