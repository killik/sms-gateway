<?php

namespace App\Http\Livewire\Dashboard\Contact\Group;

use App\Models\Auth\User;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public User $user;

    public function mounth(User $user): void
    {
        $this->user = $user;
    }

    public function render()
    {
        $groups = $this->user->contactGroups()->orderByDesc('id')->paginate(8);

        return view('livewire.dashboard.contact.group.table', compact('groups'));
    }
}
