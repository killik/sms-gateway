<?php

namespace App\Http\Livewire\Dashboard\Cards;

use App\Models\Message\Outbox;
use Livewire\Component;

class Message extends Component
{
    public Outbox $message;

    protected function mounth(Outbox $message): void
    {
        $this->message = $message;
    }

    public function render()
    {
        return view('livewire.dashboard.cards.message');
    }
}
