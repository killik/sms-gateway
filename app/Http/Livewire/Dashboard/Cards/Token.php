<?php

namespace App\Http\Livewire\Dashboard\Cards;

use App\Models\Auth\User;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Token extends Component
{
    public User $user;

    public function mounth(User $user): Void
    {
        $this->user = $user;
    }

    public function new(): void
    {
        $this->user->makeApiToken()->update();
    }

    public function render(): View
    {
        return view('livewire.dashboard.cards.token');
    }
}
