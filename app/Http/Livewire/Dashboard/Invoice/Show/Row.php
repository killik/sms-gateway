<?php

namespace App\Http\Livewire\Dashboard\Invoice\Show;

use App\Models\Auth\User;
use App\Models\Invoice\Invoice;
use Barryvdh\DomPDF\Facade as PDF;
use Dompdf\Dompdf;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Symfony\Component\HttpFoundation\StreamedResponse;

class Row extends Component
{
    public Invoice $invoice;
    public User $user;

    protected $listeners = [

        'refresh' => '$refresh'
    ];

    public function mounth(Invoice $invoice, User $user): void
    {
        $this->invoice = $invoice;
        $this->user = $user;
    }

    public function render(): View
    {
        return view('livewire.dashboard.invoice.show.row');
    }
}
