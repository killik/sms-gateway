<?php

namespace App\Http\Livewire\Dashboard\Invoice\Show\Products;

use App\Models\Auth\User;
use App\Models\Invoice\Invoice;
use Livewire\Component;

class Table extends Component
{
    public Invoice $invoice;

    public function mount(Invoice $invoice): void
    {
        $this->invoice     = $invoice;
    }

    public function render()
    {
        return view('livewire.dashboard.invoice.show.products.table');
    }
}
