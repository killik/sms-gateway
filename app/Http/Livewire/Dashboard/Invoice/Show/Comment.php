<?php

namespace App\Http\Livewire\Dashboard\Invoice\Show;

use App\Models\Auth\User;
use App\Models\Invoice\Comment as InvoiceComment;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Comment extends Component
{
    public InvoiceComment $comment;
    public User $user;

    public function mounth(InvoiceComment $comment, User $user): void
    {
        $this->comment = $comment;
        $this->user = $user;
    }

    public function render(): View
    {
        return view('livewire.dashboard.invoice.show.comment');
    }
}
