<?php

namespace App\Http\Livewire\Dashboard\Invoice\Show;

use App\Models\Invoice\Invoice;
use Livewire\Component;
use Livewire\WithFileUploads;

class Pay extends Component
{
    use WithFileUploads;

    public $photo;

    public Invoice $invoice;

    public ?String $message = '', $modal;

    protected $rules = [

        'photo' => 'required|image|max:2048',
        'message' => 'nullable|string|max:160'
    ];

    public function mounth(Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }

    public function updated(string $field): void
    {
        $this->validateOnly($field);
    }

    public function resetForm(): void
    {
        unset($this->photo, $this->message);
    }

    public function submit(): void
    {
        $this->validate();

        $attachment = $this->invoice->user->documents()->create([

            'name' => sprintf('invoice_%s', $this->invoice->ref),
            'type' => 'image',
            'path' => $this->photo->storeAs('public/invoice', $this->invoice->ref, 'local'),
            'description' => $this->message
        ]);

        $this->invoice->attachment()->associate($attachment);

        $this->invoice->process();

        $this->emitTo('dashboard.invoice.show.row', 'refresh');

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render()
    {
        return view('livewire.dashboard.invoice.show.pay');
    }
}
