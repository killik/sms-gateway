<?php

namespace App\Http\Livewire\Dashboard\Invoice\Show;

use App\Models\Auth\User;
use App\Models\Invoice\Invoice;
use Livewire\Component;

class Comments extends Component
{
    public Invoice $invoice;
    public User $user;

    public ?String $form = "";

    protected $rules = [

        'form' => 'required|string|max:160'
    ];

    protected $listeners = [

        'refresh' => '$refresh'
    ];

    public function resetForm(): void
    {
        unset($this->form);

        $this->resetErrorBag();
    }

    public function mounth(Invoice $invoice, User $user): void
    {
        $this->invoice = $invoice;
        $this->user = $user;
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $comment = $this->invoice->comments()->create([

            'body' => $this->form,
            'user_id' => $this->user->id
        ]);

        $comment->readers()->attach($this->user->id);

        $this->resetForm();

        $this->refresh();
    }

    public function refresh(): void
    {
        $this->emit('refresh');
    }

    public function render()
    {
        $comments = $this->invoice->comments()->orderByDesc('id')->get();

        return view('livewire.dashboard.invoice.show.comments', compact('comments'));
    }
}
