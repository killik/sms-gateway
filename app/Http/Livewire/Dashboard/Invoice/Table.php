<?php

namespace App\Http\Livewire\Dashboard\Invoice;

use App\Models\Auth\User;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public ?String  $status;

    public User     $user;

    protected $paginationTheme = 'bootstrap';

    public function mount(User $user, ?string $status): void
    {
        $this->status   = $status;
        $this->user     = $user;
    }

    public function render()
    {
        $invoices = $this->user->invoices();

        if (!empty($this->status))
        {
            $invoices->where('status', $this->status);
        }

        $invoices = $invoices->orderByDesc('id')->paginate(8);

        return view('livewire.dashboard.invoice.table', compact('invoices'));
    }
}
