<?php

namespace App\Http\Livewire\Dashboard\Header;

use App\Models\Auth\User;
use App\Models\Invoice\Comment;
use App\Models\Invoice\Invoice;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Message extends Component
{
    public User $user;

    public function mount(User $user): void
    {
        $this->user = $user;
    }

    public function read(Invoice $invoice): void
    {
        $comments = $invoice->comments()->whereDoesntHave('readers', fn ($readers) => $readers->where('id', $this->user->id))->get();
        $data = $comments->map(fn (Comment $comment) => [
            'user_id' => $this->user->id,
            'comment_id' => $comment->id
        ]);

        DB::table('invoice_comment_reader')->insert($data->all());

        $this->redirectRoute('dashboard.invoice.show', ['invoice' => $invoice->ref]);
    }

    public function getInvoices()
    {
        $invoices = $this->user->invoices()->whereHas('comments', function ($comments)
        {
            $comments->whereDoesntHave('readers', function ($readers)
            {
                $readers->where('id', $this->user->id);
            });
        });

        return $invoices->orderBy('created_at', 'DESC')->get();
    }

    public function render()
    {
        $invoices = $this->getInvoices();

        return view('livewire.dashboard.header.message', compact('invoices'));
    }
}
