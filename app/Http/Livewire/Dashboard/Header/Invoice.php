<?php

namespace App\Http\Livewire\Dashboard\Header;

use App\Models\Auth\User;
use Livewire\Component;

class Invoice extends Component
{
    public User $user;

    public function mount(User $user): void
    {
        $this->user = $user;
    }

    public function render()
    {
        $invoice = $this->user->invoices()->where('status', 'pending')->count();

        return view('livewire.dashboard.header.invoice', compact('invoice'));
    }
}
