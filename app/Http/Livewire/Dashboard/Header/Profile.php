<?php

namespace App\Http\Livewire\Dashboard\Header;

use Livewire\Component;

class Profile extends Component
{
    public function render()
    {
        return view('livewire.dashboard.header.profile');
    }
}
