<?php

namespace App\Http\Livewire\Dashboard\Profile;

use App\Models\Auth\User;
use Livewire\Component;

class Row extends Component
{
    protected $listeners = ['userUpdated' => 'reMount'];

    public User $user;

    public function mount(User $user): void
    {
        $this->user = $user;
    }

    public function reMount(): void
    {
        $this->mount($this->user);
    }

    public function render()
    {
        return view('livewire.dashboard.profile.row');
    }
}
