<?php

namespace App\Http\Livewire\Dashboard\Profile\Form;

use App\Models\Auth\User;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Update extends Component
{
    public  String  $email, $name, $modal;
    public  User    $user;

    protected   array   $rules = [

        'name'  => 'required|min:6',
        'email' => 'required|email'
    ];

    public function mount(User $user, string $modal): void
    {
        $this->name     = $user->name;
        $this->email    = $user->email;

        $this->modal    = $modal;

        $this->user     = $user;
    }

    public function resetForm()
    {
        $this->name     = $this->user->name;
        $this->email    = $this->user->email;

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $this->user->update([

            'name'  => $this->name,
            'email' => $this->email
        ]);

        $this->emit('userUpdated');

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render(): View
    {
        return view('livewire.dashboard.profile.form.update');
    }
}
