<?php

namespace App\Http\Livewire\Dashboard\Profile\Form;

use App\Models\Auth\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Cpass extends Component
{
    public  String  $password = '', $n_password = '', $c_password = '', $modal;
    public  User    $user;

    protected   array   $rules = [

        'password'      => 'required|password',
        'n_password'    => 'required|min:6',
        'c_password'    => 'required_with:password|same:n_password'
    ];

    public function mount(User $user, string $modal): void
    {
        $this->user     = $user;

        $this->modal    = $modal;
    }

    public function resetForm()
    {
        $this->n_password   = '';
        $this->c_password   = '';
        $this->password     = '';

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate(null, [], [

            'n_password' => 'new password',
            'c_password' => 'password confirmation'
        ]);

        $this->user->update(['password' => Hash::make($this->password)]);

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render(): View
    {
        return view('livewire.dashboard.profile.form.cpass');
    }
}
