<?php

namespace App\Http\Livewire\Dashboard\Profile\Form;

use App\Jobs\Generic\Notification\NotifySuperUsers;
use App\Jobs\SMS\Notif;
use App\Models\Auth\User;
use App\Models\Product\Product;
use App\Notifications\Admin\Invoice\Created;
use App\Notifications\Dashboard\Invoice\Created as InvoiceCreated;
use Illuminate\Support\Collection;
use Livewire\Component;

class Quota extends Component
{
    public  int     $selected;
    public  String  $modal;
    public  User    $user;
    public Collection $products;

    protected   array   $rules = [

        'selected' => 'required|exists:product_products,id',
    ];

    public function __construct($id = null)
    {
        parent::__construct($id);
    }

    public function getPriceProperty(): int
    {
        return $this->products->where('id', $this->selected)->first()->price ?? 0;
    }

    public function mount(User $user, string $modal): void
    {
        $this->products = Product::whereActive(true)->get();
        $this->user     = $user;
        $this->selected = $this->products->first()->id ?? 0;

        $this->modal = $modal;
    }

    public function resetForm()
    {
        $this->quantity = 100;

        $this->resetErrorBag();
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }

    public function submit(): void
    {
        $this->validate();

        $invoice = $this->user->invoices()->create();
        $invoice->products()->attach($this->selected);
        $user = $invoice->user;

        $user->notify(new InvoiceCreated($invoice));

        NotifySuperUsers::dispatch(new Created($invoice))->onQueue('generic');

        $this->emit('invoiceCreated');

        $this->emit('bootstrapModalHide', $this->modal);
    }

    public function render()
    {
        return view('livewire.dashboard.profile.form.quota');
    }
}
