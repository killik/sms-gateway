<?php

namespace App\Http\Livewire\Dashboard\Outbox;

use App\Models\Whatsapp\Outbox;
use Livewire\Component;

class MessageWhatsapp extends Component
{
    public Outbox $message;

    public function mounth(Outbox $message): void
    {
        $this->message = $message;
    }

    public function send(): void
    {
        $this->message->send();
    }

    public function render()
    {
        return view('livewire.dashboard.outbox.message-whatsapp');
    }
}
