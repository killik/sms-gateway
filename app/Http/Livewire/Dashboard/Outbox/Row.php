<?php

namespace App\Http\Livewire\Dashboard\Outbox;

use App\Jobs\SMS\Outbox\Reload;
use App\Models\Auth\User;
use App\Models\Message\Outbox;
use Livewire\Component;
use Livewire\WithPagination;

class Row extends Component
{
    use WithPagination;

    public ?String  $status;

    public User     $user;

    public String  $keyword = '';

    protected $paginationTheme = 'bootstrap';

    public function mouth(User $user, ?string $status)
    {
        $this->user     = $user;
        $this->status   = $status;
    }

    public function render()
    {
        $outbox = $this->user->outbox();

        if (!empty($this->status))
        {
            $outbox->where('status', $this->status);
        }

        if (!empty($this->keyword))
        {
            $outbox->where(function ($query)
            {

                $query->where('destination', 'LIKE', '%' . $this->keyword . '%');
                $query->orWhere('message', 'LIKE', '%' . $this->keyword . '%');

                $query->orWhereHas('contact', function ($query)
                {
                    $query->where('name', 'LIKE', '%' . $this->keyword . '%');
                });
            });
        }

        $outbox = $outbox->orderBy('id', 'DESC')->paginate(9);

        // $outbox->each(function (Outbox $outbox) {

        //     if ($outbox->status == 'pending')
        //     {
        //         Reload::dispatch($outbox, true)->onQueue('message:reload2');
        //     }
        // });

        return view('livewire.dashboard.outbox.row', compact('outbox'));
    }
}
