<?php

namespace App\Http\Livewire\Dashboard\Outbox;

use App\Models\Message\Outbox;
use Livewire\Component;

class Message extends Component
{
    public Outbox $message;

    public function mounth(Outbox $message): void
    {
        $this->message = $message;
    }

    public function reload(): void
    {
        $this->message->reload();
    }

    public function send(): void
    {
        $this->message->send();
    }

    public function render()
    {
        return view('livewire.dashboard.outbox.message');
    }
}
