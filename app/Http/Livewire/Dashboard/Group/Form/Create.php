<?php

namespace App\Http\Livewire\Dashboard\Group\Form;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.dashboard.group.form.create');
    }
}
