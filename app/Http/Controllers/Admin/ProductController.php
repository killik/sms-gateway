<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, string $status = null): View
    {
        $user = $request->user();

        return view('admin.product.index', compact('status', 'user'));
    }

    public function active(Request $request): View
    {
        return $this->index($request, 'active');
    }

    public function inactive(Request $request): View
    {
        return $this->index($request, 'inactive');
    }

    public function home(Request $request): View
    {
        return $this->index($request, 'processing');
    }

    public function show(Request $request, Product $product)
    {
        $user = $request->user();

        return view('admin.product.show', compact('product', 'user'));
    }
}
