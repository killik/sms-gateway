<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, string $status = null): View
    {
        $user = $request->user();

        return view('admin.invoice.index', compact('status', 'user'));
    }

    public function home(Request $request): View
    {
        return $this->index($request, 'processing');
    }

    public function show(Request $request, Invoice $invoice)
    {
        $user = $request->user();

        return view('admin.invoice.show', compact('invoice', 'user'));
    }
}
