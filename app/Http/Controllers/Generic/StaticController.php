<?php

namespace App\Http\Controllers\Generic;

use App\Http\Controllers\Controller;
use App\Services\Generic\StaticPage;

class StaticController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StaticPage $staticPage, string $path = 'index')
    {
        return $staticPage->respond($path, resource_path('views/static'));
    }
}
