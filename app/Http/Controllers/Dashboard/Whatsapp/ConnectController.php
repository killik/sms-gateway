<?php

namespace App\Http\Controllers\Dashboard\Whatsapp;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ConnectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, string $status = null): View
    {
        $user = $request->user();

        return view('dashboard.whatsapp.connect', compact('user'));
    }
}
