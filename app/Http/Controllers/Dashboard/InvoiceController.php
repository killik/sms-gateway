<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Invoice\ShowRequest;
use App\Models\Invoice\Invoice;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, string $status = null): View
    {
        $user = $request->user();

        return view('dashboard.invoice.index', compact('status', 'user'));
    }

    public function show(ShowRequest $request, Invoice $invoice)
    {
        $user = $request->user();

        return view('dashboard.invoice.show', compact('invoice', 'user'));
    }

    public function print(ShowRequest $request, Invoice $invoice)
    {
        $user = $request->user();

        return view('dashboard.invoice.print', compact('invoice', 'user'));
    }
}
