<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\EndpointResource;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Illuminate\Support\Str;

class EndpointController extends Controller
{
    public function __invoke(Router $router, string $prefix)
    {
        $prefix = Str::start($prefix, 'api.');

        $endpoint = collect($router->getRoutes())->map(fn (Route $route) => $this->getRouteInformation($route))

            ->reject(fn (array $route) => !Str::startsWith($route['name'], $prefix))

            ->map(fn (array $route) => array_merge($route, ['name' => Str::after($route['name'], Str::start('.', $prefix))]));

        return EndpointResource::collection($endpoint);
    }

    /**
     * Get the route information for a given route.
     *
     * @param  \Illuminate\Routing\Route  $route
     * @return array
     */
    protected function getRouteInformation(Route $route)
    {
        return [
            'domain' => $route->domain(),
            'methods' => $route->methods(),
            'uri'    => $route->uri(),
            'name'   => $route->getName()
        ];
    }
}
