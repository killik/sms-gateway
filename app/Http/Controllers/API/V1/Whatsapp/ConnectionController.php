<?php

namespace App\Http\Controllers\API\V1\Whatsapp;

use App\Helpers\Pager\Pager;
use App\Http\Controllers\Controller;
use App\Services\Whatsapp\Connection;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ConnectionController extends Controller
{
    protected Connection $conn;

    public function status(Request $request)
    {
        $this->conn = new Connection($request->user()->api_token);

        try {
            $this->conn->getStatus();
            return response()->json([
                'message' => 'Whatsapp connected',
                'status' => 200
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()
                ->json([
                    'message' => $th->getMessage(),
                    'status' => $th->getCode() ?: 500
                ])
                ->setStatusCode($th->getCode() ?: 500);
        }
    }

    public function connect(Request $request)
    {
        // Use manual curl because GuzzleHttp not work
        try {
            $token = $request->user()->api_token;
            $url = config('app.whatsapp_api_url') . '/connection/qr';
    
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    
            $headers = array(
                "Accept: application/json",
                "Authorization: Bearer $token",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    
            $resp = curl_exec($curl);
            curl_close($curl);
    
            if ($resp) return response()->json(json_decode($resp));
            else return response()
                ->json([
                    'message' => 'Looks like the whatsapp server is not respond',
                    'status' => 500
                ])
                ->setStatusCode(500);
        } catch (\Throwable $th) {
            //throw $th;
            return response()
                ->json([
                    'message' => $th->getMessage(),
                    'status' => 500
                ])
                ->setStatusCode(500);
        }
    }

    public function disconnect(Request $request)
    {
        $this->conn = new Connection($request->user()->api_token);
        
        try {
            $resp = $this->conn->close();
            return response()
                ->json([
                    'message' => "Whatsapp connection successfuly closed",
                    'status' => 200
                ])
                ->setStatusCode($resp->getStatusCode());
        } catch (\Throwable $th) {
            //throw $th;
            return response()
                ->json([
                    'message' => $th->getMessage(),
                    'status' => $th->getCode() ?: 500
                ])
                ->setStatusCode($th->getCode() ?: 500);
        }
    }
}
