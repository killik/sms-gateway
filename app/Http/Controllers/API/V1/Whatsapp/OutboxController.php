<?php

namespace App\Http\Controllers\API\V1\Whatsapp;

use App\Helpers\Pager\Pager;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\SMS\Outbox\BulkRequest;
use App\Http\Requests\API\V1\SMS\Outbox\DestroyRequest;
use App\Http\Requests\API\V1\SMS\Outbox\IndexRequest;
use App\Http\Requests\API\V1\SMS\Outbox\RetryRequest;
use App\Http\Requests\API\V1\SMS\Outbox\ShowRequest;
use App\Http\Requests\API\V1\SMS\Outbox\StoreRequest;
use App\Http\Resources\API\V1\SMS\OutboxResource;
use App\Jobs\SMS\Outbox\Bulk;
use App\Models\Whatsapp\Outbox;
use Illuminate\Database\Eloquent\Builder;

class OutboxController extends Controller
{
    public function index(IndexRequest $request): array
    {
        $outbox = $request->user()->outbox_whatsapp()->orderBy('id', 'DESC');

        if ($request->has('status'))
        {
            if ($request->status == 'notsent')
            {
                $outbox->where('status', '!=', 'sent');
            }

            else
            {
                $outbox->where('status', $request->status);
            }
        }

        if ($request->has('query'))
        {
            $outbox->where(function (Builder $query) use ($request)
            {
                $query->where('message', 'LIKE', '%' . $request->get('query') . '%');
                $query->orWhere('destination', 'LIKE', '%' . $request->get('query') . '%');
            });
        }

        $pager      = Pager::make($outbox->count(), $request->page ?? 1, $request->limit);
        $outbox     = $outbox->limit($pager['limit'])->offset($pager['offset'])->get();
        $data       = OutboxResource::collection($outbox);

        return compact('data', 'pager');
    }

    public function show(ShowRequest $request, Outbox $outbox): OutboxResource
    {
        unset($request); // Only for authorization

        return new OutboxResource($outbox);
    }

    public function store(StoreRequest $request): OutboxResource
    {
        if ($request->user()->wa < 1)
        {
            return abort(402);
        }

        $outbox = $request->user()->outbox_whatsapp()->create($request->only('destination', 'message'));

        return new OutboxResource($outbox);
    }

    public function destroy(DestroyRequest $request, Outbox $outbox): void
    {
        unset($request); // Only for authorization

        $outbox->delete();
    }

    public function retry(RetryRequest $request, Outbox $outbox)
    {
        unset($request); // Only for authorization

        $outbox->send();

        return new OutboxResource($outbox);
    }
}
