<?php

namespace App\Http\Controllers\API\V1\Whatsapp;

use App\Helpers\Pager\Pager;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\SMS\Broadcast\ShowRequest;
use App\Http\Requests\API\V1\SMS\Broadcast\StoreRequest;
use App\Http\Requests\API\V1\SMS\Outbox\IndexRequest;
use App\Http\Resources\API\V1\SMS\BroadcastResource;
use App\Http\Resources\API\V1\SMS\OutboxResource;
use App\Models\Whatsapp\Broadcast;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request): array
    {
        $broadcasts = $request->user()->broadcasts_whatsapp();

        if ($request->has('status'))
        {
            $broadcasts->where('status', $request->status);
        }

        if ($request->has('query'))
        {
            $broadcasts->where(function (Builder $query) use ($request)
            {
                $query->where('message', 'LIKE', '%' . $request->get('query') . '%');
                $query->orWhere('destinations', 'LIKE', '%' . $request->get('query') . '%');
            });
        }

        $pager      = Pager::make($broadcasts->count(), $request->page ?? 1, $request->limit);
        $messages   = $broadcasts->limit($pager['limit'])->offset($pager['offset'])->get();
        $data       = BroadcastResource::collection($messages);

        return compact('data', 'pager');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request): BroadcastResource
    {
        $user           = $request->user();
        $destinations   = collect($request->destinations);

        if ($request->user()->wa < $destinations->count())
        {
            return abort(402);
        }

        $broadcast = $user->broadcasts_whatsapp()->create([

            'destinations'  => $destinations,
            'message'       => $request->message
        ]);

        return new BroadcastResource($broadcast);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, Broadcast $broadcast)
    {
        unset($request); // Only for authorization

        return new OutboxResource($broadcast);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function retry(ShowRequest $request, Broadcast $broadcast): BroadcastResource
    {
        unset($request); // Only for authorization

        $broadcast->send();

        return new BroadcastResource($broadcast);
    }
}
