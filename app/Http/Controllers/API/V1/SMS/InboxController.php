<?php

namespace App\Http\Controllers\API\V1\SMS;

use App\Helpers\Pager\Pager;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\IndexRequest;
use App\Http\Resources\API\V1\SMS\InboxResource;
use App\Jobs\SMS\Inbox\Sync;
use App\Models\Generic\Job\Queued;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class InboxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        if (Queued::where('queue', 'message:inbox:sync')->count() < 4)
        {
            Sync::dispatch()->onQueue('message:inbox:sync')->delay(now()->addSeconds(5));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request): array
    {
        $inbox = $request->user()->inbox();

        if ($request->has('status'))
        {
            $inbox->where('status', $request->status);
        }

        if ($request->has('query'))
        {
            $inbox->where(function (Builder $query) use ($request)
            {
                $query->where('message', 'LIKE', '%' . $request->get('query') . '%');
                $query->orWhere('sender', 'LIKE', '%' . $request->get('query') . '%');
            });
        }

        $pager      = Pager::make($inbox->count(), $request->page ?? 1, $request->limit);
        $messages   = $inbox->limit($pager['limit'])->offset($pager['offset'])->get();
        $data       = InboxResource::collection($messages);

        return compact('data', 'pager');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
