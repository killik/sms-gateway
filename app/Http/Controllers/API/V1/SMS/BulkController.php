<?php

namespace App\Http\Controllers\API\V1\SMS;

use App\Helpers\Pager\Pager;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\SMS\Bulk\ShowRequest;
use App\Http\Requests\API\V1\SMS\Bulk\StoreRequest;
use App\Http\Requests\API\V1\SMS\Outbox\IndexRequest;
use App\Http\Resources\API\V1\SMS\BulkResource;
use App\Http\Resources\API\V1\SMS\OutboxResource;
use App\Models\Message\Bulk;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BulkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request): array
    {
        $bulks = $request->user()->bulks();

        if ($request->has('status'))
        {
            $bulks->where('status', $request->status);
        }

        if ($request->has('query'))
        {
            $bulks->where(function (Builder $query) use ($request)
            {
                $query->where('message', 'LIKE', '%' . $request->get('query') . '%');
                $query->orWhere('destinations', 'LIKE', '%' . $request->get('query') . '%');
            });
        }

        $pager      = Pager::make($bulks->count(), $request->page ?? 1, $request->limit);
        $messages   = $bulks->limit($pager['limit'])->offset($pager['offset'])->get();
        $data       = BulkResource::collection($messages);

        return compact('data', 'pager');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request): BulkResource
    {
        $user           = $request->user();
        $destinations   = collect($request->destinations);

        if ($request->user()->sms < $destinations->count())
        {
            return abort(402);
        }

        $bulk = $user->bulks()->create($request->only('messages'));

        return new BulkResource($bulk);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, Bulk $bulk)
    {
        unset($request); // Only for authorization

        return new OutboxResource($bulk);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function retry(ShowRequest $request, Bulk $bulk): BulkResource
    {
        unset($request); // Only for authorization

        $bulk->send();

        return new BulkResource($bulk);
    }
}
