<?php

namespace App\Http\Controllers\API\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\V1\User\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request): UserResource
    {
        return new UserResource($request->user());
    }
}
