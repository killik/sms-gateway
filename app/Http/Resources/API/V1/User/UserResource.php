<?php

namespace App\Http\Resources\API\V1\User;

use App\Models\Auth\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function __construct(User $resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        $data['outbox']     = $this->resource->outbox()->count();
        $data['sent']       = $this->resource->outbox()->where('status', 'sent')->count();
        $data['failed']     = $this->resource->outbox()->where('status', 'failed')->count();
        $data['pending']    = $this->resource->outbox()->where('status', 'pending')->count();

        return $data;
    }
}
