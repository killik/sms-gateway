<?php

namespace App\Http\Routes\API\V1;

use App\Http\Routes\API\V1\SMS\Broadcast;
use App\Http\Routes\API\V1\SMS\Bulk;
use App\Http\Routes\API\V1\SMS\Inbox;
use App\Http\Routes\API\V1\SMS\Outbox;
use App\Http\Routes\Base;

class SMS extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Inbox::register();
        Outbox::register();
        Broadcast::register();
        Bulk::register();
    }
}
