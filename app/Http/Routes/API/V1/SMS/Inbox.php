<?php

namespace App\Http\Routes\API\V1\SMS;

use App\Http\Controllers\API\V1\SMS\InboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Inbox extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {
        //
    }

    protected static function registerRoutes(): void
    {
        Route::apiResource('inbox', InboxController::class);
    }
}
