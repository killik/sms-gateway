<?php

namespace App\Http\Routes\API\V1\SMS;

use App\Http\Controllers\API\V1\SMS\OutboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Outbox extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {

        Route::prefix('{outbox}')->group(function ()
        {
            Route::get('reload', [OutboxController::class, 'reload'])->name('reload');
            Route::get('retry', [OutboxController::class, 'retry'])->name('retry');
        });
    }

    protected static function registerRoutes(): void
    {
        Route::apiResource('outbox', OutboxController::class);
    }
}
