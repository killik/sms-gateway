<?php

namespace App\Http\Routes\API\V1\SMS;

use App\Http\Controllers\API\V1\SMS\BulkController;
use App\Http\Controllers\API\V1\SMS\OutboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Bulk extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {
        Route::prefix('{bulk}')->group(function ()
        {
            Route::get('retry', [OutboxController::class, 'retry'])->name('retry');
        });
    }

    protected static function registerRoutes(): void
    {
        Route::apiResource('bulk', BulkController::class);
    }
}
