<?php

namespace App\Http\Routes\API\V1\Whatsapp;

use App\Http\Controllers\API\V1\Whatsapp\ConnectionController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Connection extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {
        Route::get('status', [ConnectionController::class, 'status'])->name('status');
        Route::get('connect', [ConnectionController::class, 'connect'])->name('connect');
        Route::get('disconnect', [ConnectionController::class, 'disconnect'])->name('disconnect');
    }
}
