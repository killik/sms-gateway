<?php

namespace App\Http\Routes\API\V1\Whatsapp;

use App\Http\Controllers\API\V1\Whatsapp\BroadcastController;
use App\Http\Controllers\API\V1\Whatsapp\OutboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Broadcast extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {
        Route::prefix('{broadcast}')->group(function ()
        {
            Route::get('retry', [OutboxController::class, 'retry'])->name('retry');
        });
    }

    protected static function registerRoutes(): void
    {
        Route::apiResource('broadcast', BroadcastController::class);
    }
}
