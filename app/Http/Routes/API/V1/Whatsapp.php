<?php

namespace App\Http\Routes\API\V1;

use App\Http\Routes\API\V1\Whatsapp\Connection;
use App\Http\Routes\API\V1\Whatsapp\Bulk;
use App\Http\Routes\API\V1\Whatsapp\Broadcast;
use App\Http\Routes\API\V1\Whatsapp\Outbox;
use App\Http\Routes\Base;
class Whatsapp extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {
        Connection::register();
        Outbox::register();
        Broadcast::register();
        Bulk::register();
    }
}
