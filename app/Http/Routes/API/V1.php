<?php

namespace App\Http\Routes\API;

use App\Http\Controllers\API\V1\User\UserController;
use App\Http\Routes\API\V1\SMS;
use App\Http\Routes\API\V1\Whatsapp;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class V1 extends Base
{
    protected static function registerPrefixedNamedRoutes(): void
    {
        Route::middleware('auth:api')->group(fn () => static::registerAuthRequiredRoutes());
    }

    protected static function registerAuthRequiredRoutes(): void
    {
        SMS::register();
        Whatsapp::register();

        Route::get('user', [UserController::class, 'index']);
    }

    protected static function registerRoutes(): void
    {
        //
    }
}
