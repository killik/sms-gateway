<?php

namespace App\Http\Routes;

use App\Http\Controllers\API\EndpointController;
use App\Http\Routes\API\V1;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class API extends Base
{
    protected static function registerNamedRoutes(): void
    {
        V1::register();
    }

    protected static function registerRoutes(): void
    {
        Route::get('endpoint/{prefix}', EndpointController::class)->name('endpoint');
    }
}
