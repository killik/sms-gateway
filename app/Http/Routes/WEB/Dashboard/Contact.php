<?php

namespace App\Http\Routes\WEB\Dashboard;

use App\Http\Routes\Base;
use App\Http\Routes\WEB\Dashboard\Contact\Contact as ContactContact;
use App\Http\Routes\WEB\Dashboard\Contact\Group;

class Contact extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        ContactContact::register();
        Group::register();
    }

    public static function registerRoutes(): void
    {
        //
    }
}
