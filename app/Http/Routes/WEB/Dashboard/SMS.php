<?php

namespace App\Http\Routes\WEB\Dashboard;

use App\Http\Routes\Base;
use App\Http\Routes\WEB\Dashboard\SMS\Inbox;
use App\Http\Routes\WEB\Dashboard\SMS\Outbox;

class SMS extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Outbox::register();
        Inbox::register();
    }
}
