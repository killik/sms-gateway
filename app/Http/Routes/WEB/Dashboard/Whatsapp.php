<?php

namespace App\Http\Routes\WEB\Dashboard;

use App\Http\Controllers\Dashboard\Whatsapp\ConnectController;
use App\Http\Controllers\Dashboard\Whatsapp\SendController;
use App\Http\Routes\Base;
use App\Http\Routes\WEB\Dashboard\Whatsapp\Outbox;
use Illuminate\Support\Facades\Route;
class Whatsapp extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Outbox::register();
        Route::get('connect', [ConnectController::class, 'index'])->name('connect');
        Route::get('send', [SendController::class, 'index'])->name('send');
    }
}
