<?php

namespace App\Http\Routes\WEB\Dashboard\Whatsapp;

use App\Http\Controllers\Dashboard\Whatsapp\OutboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Outbox extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::get('{status?}', [OutboxController::class, 'index'])->name('index');
    }
}
