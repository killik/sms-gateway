<?php

namespace App\Http\Routes\WEB\Dashboard\Contact;

use App\Http\Controllers\Dashboard\Contact\ContactController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Contact extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        //
    }

    public static function registerRoutes(): void
    {
        Route::resource('contact', ContactController::class);
    }
}
