<?php

namespace App\Http\Routes\WEB\Dashboard;

use App\Http\Controllers\Dashboard\InvoiceController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Invoice extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::get('print/{invoice:ref}', [InvoiceController::class, 'print'])->name('print')->where('invoice', '^\w{9}$');
        Route::get('{invoice:ref}', [InvoiceController::class, 'show'])->name('show')->where('invoice', '^\w{9}$');
        Route::get('{status?}', [InvoiceController::class, 'index'])->name('index');
    }
}
