<?php

namespace App\Http\Routes\WEB\Dashboard;

use App\Http\Controllers\Dashboard\ProductController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Product extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::get('', [ProductController::class, 'index'])->name('index');
    }
}
