<?php

namespace App\Http\Routes\WEB\Dashboard\SMS;

use App\Http\Controllers\Dashboard\SMS\OutboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Outbox extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::get('{status?}', [OutboxController::class, 'index'])->name('index');
    }
}
