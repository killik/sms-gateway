<?php

namespace App\Http\Routes\WEB\Dashboard\SMS;

use App\Http\Controllers\Dashboard\SMS\InboxController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Inbox extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::get('{status?}', [InboxController::class, 'index'])->name('index');
    }
}
