<?php

namespace App\Http\Routes\WEB\Dashboard;

use App\Http\Controllers\Dashboard\Contact\GroupController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Group extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        //
    }

    public static function registerRoutes(): void
    {
        Route::resource('group', GroupController::class);
    }
}
