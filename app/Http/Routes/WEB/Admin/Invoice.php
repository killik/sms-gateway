<?php

namespace App\Http\Routes\WEB\Admin;

use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Invoice extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::get('{invoice:ref}', [InvoiceController::class, 'show'])->name('show')->where('invoice', '^\w{9}$');
        Route::get('{status?}', [InvoiceController::class, 'index'])->name('index');
    }
}
