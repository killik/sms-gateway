<?php

namespace App\Http\Routes\WEB\Admin;

use App\Http\Controllers\Admin\ProductController;
use App\Http\Routes\Base;
use Illuminate\Support\Facades\Route;

class Product extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {

        Route::get('', [ProductController::class, 'index'])->name('index');
        Route::get('active', [ProductController::class, 'active'])->name('active');
        Route::get('inative', [ProductController::class, 'inactive'])->name('inactive');
        Route::get('{product}', [ProductController::class, 'show'])->name('show');
    }
}
