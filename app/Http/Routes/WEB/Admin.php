<?php

namespace App\Http\Routes\WEB;

use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Routes\Base;
use App\Http\Routes\WEB\Admin\Invoice;
use App\Http\Routes\WEB\Admin\Product;
use Illuminate\Support\Facades\Route;

class Admin extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {
        Route::middleware('can:access.admin')->group(function ()
        {
            Route::get('home', [InvoiceController::class, 'home'])->name('home');

            Product::register();
            Invoice::register();
        });
    }
}
