<?php

namespace App\Http\Routes\WEB;

use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Routes\Base;
use App\Http\Routes\WEB\Dashboard\Contact;
use App\Http\Routes\WEB\Dashboard\Group;
use App\Http\Routes\WEB\Dashboard\Invoice;
use App\Http\Routes\WEB\Dashboard\Product;
use App\Http\Routes\WEB\Dashboard\Whatsapp;
use App\Http\Routes\WEB\Dashboard\SMS;
use Illuminate\Support\Facades\Route;

class Dashboard extends Base
{
    public static function registerPrefixedNamedRoutes(): void
    {

        Route::get('home', [HomeController::class, 'index'])->name('home');
        Route::get('profile', [ProfileController::class, 'index'])->name('profile');

        Contact::register();
        Product::register();
        Whatsapp::register();
        Group::register();
        Invoice::register();
        SMS::register();
    }
}
