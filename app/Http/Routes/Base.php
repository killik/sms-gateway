<?php

namespace App\Http\Routes;

use Illuminate\Support\Facades\Route;
use ReflectionClass;

class Base
{
    public static final function register(string $prefix = null): void
    {
        $prefix = $prefix ?? strtolower(static::getName());

        Route::prefix($prefix)->name($prefix.'.')->group(fn () => static::registerPrefixedNamedRoutes());

        Route::prefix($prefix)->group(fn () => static::registerPrefixedRoutes());

        Route::name($prefix.'.')->group(fn () => static::registerNamedRoutes());

        static::registerRoutes();
    }

    protected static final function getName(): string
    {
        $reflection = new ReflectionClass(static::class);

        return $reflection->getShortName();
    }

    protected static function registerRoutes(): void
    {
        //
    }

    protected static function registerPrefixedRoutes(): void
    {
        //
    }

    protected static function registerNamedRoutes(): void
    {
        //
    }

    protected static function registerPrefixedNamedRoutes(): void
    {
        //
    }
}
