<?php

namespace App\Http\Routes;

use App\Http\Controllers\Generic\StaticController;
use App\Http\Routes\Base;
use App\Http\Routes\WEB\Admin;
use App\Http\Routes\WEB\Dashboard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

class WEB extends Base
{
    public static function registerRoutes(): void
    {
        Route::middleware('auth')->group(fn () => static::registerAuthRequiredRoutes());

        Auth::routes();

        Route::get('{path}', StaticController::class)->where('path', '(.*)');
    }

    protected static function registerAuthRequiredRoutes(): void
    {
        Admin::register();
        Dashboard::register();
    }
}
