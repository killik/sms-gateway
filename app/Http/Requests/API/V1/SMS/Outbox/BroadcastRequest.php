<?php

namespace App\Http\Requests\API\V1\SMS\Outbox;

use Illuminate\Foundation\Http\FormRequest;

class BroadcastRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'destinations'      => 'required|array',
            'destinations.*'    => 'required|string',
            'message'           => 'required|string'
        ];
    }
}
