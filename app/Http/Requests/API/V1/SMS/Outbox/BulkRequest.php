<?php

namespace App\Http\Requests\API\V1\SMS\Outbox;

use Illuminate\Foundation\Http\FormRequest;

class BulkRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'messages.*.destination'   => 'required|string',
            'messages.*.message'       => 'required|string'
        ];
    }
}
