<?php

namespace App\Http\Requests\API\V1\SMS\Outbox;

use App\Http\Requests\API\V1\IndexRequest as V1IndexRequest;

class IndexRequest extends V1IndexRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [

            'status' => 'nullable|in:pending,queued,sent,pending,processing,notsent',
            'query'  => 'nullable|string'
        ]);
    }
}
