<?php

namespace App\Http\Requests\API\V1\SMS\Bulk;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'messages'                  => 'required|array',
            'messages.*.destination'    => 'required|string',
            'messages.*.message'        => 'required|string'
        ];
    }
}
