<?php

namespace App\View\Components\Generic;

use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\View\Component;

class LoremIpsum extends Component
{
    public int          $count;
    public Generator    $factory;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(int $count)
    {
        $this->count    = $count;
        $this->factory  = Container::getInstance()->make(Generator::class);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.generic.lorem-ipsum');
    }
}
