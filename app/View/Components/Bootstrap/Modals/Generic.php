<?php

namespace App\View\Components\Bootstrap\Modals;

use Illuminate\View\Component;

class Generic extends Component
{
    public string $id, $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.bootstrap.modals.generic');
    }
}
