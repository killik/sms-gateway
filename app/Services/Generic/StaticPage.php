<?php

namespace App\Services\Generic;

use GuzzleHttp\Psr7\MimeType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class StaticPage
{
    protected Request $request;

    protected array $data = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function bladeData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function respond(string $uri, string $resource_path = null): ?Response
    {
        $resource_path = sprintf('%s/%s', $resource_path ??  resource_path('static'), $uri);

        if ($this->isBlade($resource_path))
        {
            return $this->bladeResponse($resource_path);
        }

        elseif (file_exists($resource_path) && !empty($resource_mime = MimeType::fromFilename($resource_path)))
        {
            $resource = file_get_contents($resource_path);

            return response($resource)->header('Content-Type', $resource_mime);
        }

        abort(404);
    }

    protected function isBlade(string $path): bool
    {
        return file_exists($path . '.blade.php');
    }

    protected function bladeResponse(string $path): Response
    {
        $view_path = Str::after($path, resource_path('views'));

        return response(view($view_path, $this->data));
    }
}
