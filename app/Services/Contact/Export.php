<?php

namespace App\Services\Contact;

use App\Models\Auth\User;
use App\Models\Contact\Contact;

class Export
{
    public static function print(User $user): void
    {
        $lines = $user->contacts->map(fn (Contact $contact) => sprintf('%s;%s', $contact->name, $contact->phone_number));

        $lines->each(fn (string $line) => print $line.PHP_EOL);
    }
}
