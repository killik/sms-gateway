<?php

namespace App\Services\Contact;

use Illuminate\Support\Collection;

class Import
{
    protected bool $valid = true;

    protected Collection $lines;

    protected function inValid(): void
    {
        $this->valid = false;
    }

    public function __construct(string $source)
    {
        $this->process($source);
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function getLines(): Collection
    {
        return $this->lines;
    }

    protected function process(string $source): void
    {
        $this->lines = collect(explode(PHP_EOL, $source))->reject(fn (string $line) => empty($line))

            ->reject(function (string $line): bool
            {
                if (!preg_match('/^[a-zA-Z]+;\+[0-9]+$/', $line))
                {
                    $this->inValid();

                    return true;
                }

                return false;
            })

            ->map(function (string $line): array
            {
                $data = explode(';', $line);

                return [

                    'name' => $data[0],
                    'number' => $data[1]
                ];
            });
    }

    public static function parse(string $source): self
    {
        return new static($source);
    }
}
