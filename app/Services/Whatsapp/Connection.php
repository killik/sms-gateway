<?php

namespace App\Services\Whatsapp;

class Connection extends Client
{
	public function getStatus()
	{
		return $this->get('/connection/status');
	}

	public function getQR()
	{
		return $this->get('/connection/qr');
	}

	public function close()
	{
		return $this->get('/connection/close');
	}
}