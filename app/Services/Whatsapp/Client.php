<?php

namespace App\Services\Whatsapp;

use GuzzleHttp\Client as Guzzle;
use Psr\Http\Message\ResponseInterface;

class Client
{
	protected $token;

	public function __construct(string $token)
	{
		$this->token = $token;
	}

	/**
	 * Send get request to whatsapp worker
	 *
	 * @param  string $url Already prefixed with worker host
	 * @return ResponseInterface
	 */
	public function get($url)
	{
		$client = new Guzzle();
		$endpoint = config('app.whatsapp_api_url') . $url;
		return $client->request('GET', $endpoint, [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->token
			]
		]);
	}
	
	/**
	 * Send post request to whatsapp worker
	 *
	 * @param  string $url Already prefixed with worker host
	 * @param  array $json
	 * @return ResponseInterface
	 */
	public function post($url, $json)
	{
		$client = new Guzzle();
		$endpoint = config('app.whatsapp_api_url') . $url;
		return $client->request('POST', $endpoint, [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->token
			],
			'json' => $json
		]);
	}
}