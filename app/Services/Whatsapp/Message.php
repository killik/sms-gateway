<?php

namespace App\Services\Whatsapp;

use Illuminate\Support\Collection;

class Message extends Client
{
	public function sendMessage(string $destination, string $message)
	{
		if (substr($destination, 0, 1) == 0) {
			$destination = '62' . substr($destination, 1, strlen($destination) - 1);
		}
		return $this->post('/message/send', compact('destination', 'message'));
	}

	public function bulkMessage(Collection $messages): Collection
	{
			$messages->map(fn (array $message) => $this->sendMessage(
				$message['destination'],
				$message['body'],
			));

			return collect($messages);
	}

	public function broadcastMessage(Collection $destinations, string $message): Collection
	{
		$messages = $destinations->map(fn (string $destination) => [
				'destination'   => $destination,
				'body'          => $message,
		]);

		return $this->bulkMessage($messages);
	}
}