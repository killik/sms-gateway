<?php

namespace App\Services\SMS;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use SMSGatewayMe\Client\ClientProvider;
use SMSGatewayMe\Client\Model\Message;
use SMSGatewayMe\Client\Model\MessageSearchResult;
use SMSGatewayMe\Client\Model\SendMessageRequest;

class SMSGateway
{
    protected Collection        $deviceIDs;
    protected ClientProvider    $clientProvider;

    public function __construct(string $key, Collection $deviceIDs)
    {
        $this->clientProvider = new ClientProvider($key);

        $this->setDeviceIDs($deviceIDs->unique());
    }

    public function sendMessage(string $destination, string $message): Message
    {
        $sendMessageRequest = new SendMessageRequest([

            'phoneNumber'   => $destination,
            'message'       => $message,
            'deviceId'      => $this->getDeviceID()
        ]);

        return collect($this->getClientProvider()->getMessageClient()->sendMessages([$sendMessageRequest]))->first();
    }

    public function bulkMessage(Collection $messages): Collection
    {
        $sendMessageRequests = $messages->map(fn (array $message) => new SendMessageRequest([

            'phoneNumber'   => $message['destination'],
            'message'       => $message['body'],
            'deviceId'      => $this->getDeviceID()
        ]));

        return collect($this->getClientProvider()->getMessageClient()->sendMessages($sendMessageRequests->all()));
    }

    public function broadcastMessage(Collection $destinations, string $message): Collection
    {
        $messages = $destinations->map(fn (string $destination) => [

            'destination'   => $destination,
            'body'          => $message,
        ]);

        return $this->bulkMessage($messages);
    }

    public function getMessage(int $id)
    {
        return $this->getClientProvider()->getMessageClient()->getMessage($id);
    }

    public function searchMessages(array $filter): MessageSearchResult
    {
        return $this->getClientProvider()->getMessageClient()->searchMessages($filter);
    }

    public function getDeviceID(): int
    {
        $id = $this->getDeviceIDs()->random();
        $used = json_decode(@file_get_contents(base_path('used_device_id.json')));

        if (!$used)
        {
            $used = [];
        }

        elseif(count($used) >= $this->getDeviceIDs()->count())
        {
            if($this->getDeviceIDs()->count() <= 1)
            {
                $used = [];
            }

            else $used = [collect($used)->last()];
        }

        if (!in_array($id, $used))
        {
            $used[] = $id;

            file_put_contents(base_path('used_device_id.json'), json_encode($used));

            return $id;
        }


        return $this->getDeviceID();
    }

    public function getDeviceIDs(): Collection
    {
        return $this->deviceIDs;
    }

    public function getClientProvider(): ClientProvider
    {
        return $this->clientProvider;
    }

    public function setDeviceIDs(Collection $IDs): self
    {
        $this->deviceIDs = $IDs;

        return $this;
    }
}
